import Foundation
import UIKit

import SwiftHEXColors

/**
 The Colors struct contains all color parameter needed in the project
 
 - note: This struct contains only static property
 - author: Michaël Minelli
 - version: 1.0.0
 */
public struct Colors {
    public static let ncilabBlue = UIColor(hex: 0x2A6096)!
    
    public static let primaryColor = Colors.ncilabBlue
    public static let accentuatedPrimaryColor = UIColor(hex: 0x00008B)!
    public static let secondaryColor = UIColor.whiteColor()
    public static let tableViewSecondaryColor = Colors.accentuatedPrimaryColor
    
    public static let menuPrimaryColor = Colors.primaryColor.colorWithAlphaComponent(0.4)
    public static let menuSecondaryColor = UIColor.blackColor()
    public static let menuTableViewSecondaryColor = Colors.tableViewSecondaryColor.colorWithAlphaComponent(0.3)
    
    public static let lineBackground1 = UIColor.whiteColor()
    public static let lineBackground2 = UIColor(white: 0.96, alpha: 1)
}