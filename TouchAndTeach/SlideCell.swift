import UIKit

import SCLAlertView

/**
 The SlideCell tableview cell is the cell of the right menu which display a slide
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class SlideCell: UITableViewCell {
    enum ShareAction: Int {
        case Send = 0, Copy = 1
    }
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btName: UIButton?
    @IBOutlet weak var btShare: UIButton!
    
    var backColor: UIColor = UIColor.clearColor()
    
    private var slideNumber: Int!
    private var tableView: UITableView!
    private var viewController: UIViewController!
    
    private var shareSlidePickerController: ShareSlidePickerController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblTitle.textColor = Colors.menuSecondaryColor
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        if let btName = self.btName {
            btName.setImage(btName.imageForState(.Normal)!.image(withColor: Colors.secondaryColor), forState: .Normal)
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /**
     Prepare cell for show a new content
     
     - parameter slideNumber: Number of the slide to show
     */
    func prepareCell(withSlideNumber slideNumber: Int, forTableView tableView: UITableView, onViewController viewController: UIViewController) {
        self.backColor = slideNumber == Global_iOS.sharedInstance.documentController.currentDocument.currentSlideIndex ? Colors.menuTableViewSecondaryColor : UIColor.clearColor()
        
        self.slideNumber = slideNumber
        self.tableView = tableView
        self.viewController = viewController
        
        self.shareSlidePickerController = viewController.storyboard?.instantiateViewControllerWithIdentifier("ShareSlidePickerController") as! ShareSlidePickerController
        
        //Verify if the slide exist or show the add button
        if let paintView = Global_iOS.sharedInstance.documentController.currentDocument.getSlide(at: slideNumber) {
            self.img.image = paintView.image
            
            //Test for compatibility with old dev version of document
            if paintView.name == nil {
                paintView.name = "\(NSLocalizedString("SLIDE", comment: "SLIDE")) \(slideNumber+1)"
            }
            
            self.lblTitle.text = paintView.name!
        } else {
            self.img.backgroundColor = UIColor.whiteColor()
            self.img.image = UIImage(named: "iconAddSlide")?.image(withColor: Colors.primaryColor)
            self.lblTitle.text = NSLocalizedString("SLIDE_ADD", comment: "SLIDE_ADD")
        }
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        self.backgroundColor = highlighted ? Colors.menuTableViewSecondaryColor : backColor
    }
    
    @IBAction func btDeleteClick(sender: UIButton) {
        //Verify that they are at least two slide for accept delete of one
        if Global_iOS.sharedInstance.documentController.currentDocument.count > 1 {
            //Ask the user if he is sure to want to delete
            let alertView = SCLAlertView(appearance: SCLAlertView.SCLAppearance(showCircularIcon: true))
            
            alertView.addButton(NSLocalizedString("YES", comment: "YES")) {
                if Global_iOS.sharedInstance.documentController.currentDocument.count > 1 {
                    if let slideNumber = self.slideNumber {
                        Global_iOS.sharedInstance.documentController.currentDocument.removeSlide(at: slideNumber)
                        (self.superview!.superview! as! UITableView).reloadData()
                    }
                }
            }
            
            alertView.showCustom(NSLocalizedString("SLIDE_REMOVE_TITLE", comment: "SLIDE_REMOVE_TITLE"), subTitle: NSLocalizedString("SLIDE_REMOVE_MSG", comment: "SLIDE_REMOVE_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconDelete")!, closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
        } else {
            SCLAlertView().showCustom(NSLocalizedString("SLIDE_REMOVE_IMPOSSIBLE_TITLE", comment: "SLIDE_REMOVE_IMPOSSIBLE_TITLE"), subTitle: NSLocalizedString("SLIDE_REMOVE_IMPOSSIBLE_MSG", comment: "SLIDE_REMOVE_IMPOSSIBLE_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconDelete")!, closeButtonTitle: NSLocalizedString("OK", comment: "OK"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
        }
    }
    
    @IBAction func btNameClick(sender: UIButton) {
        Global_iOS.sharedInstance.documentController.currentDocument.getSlide(at: self.slideNumber)?.rename(withCompletionHandler: self.paintViewRenamed)
    }
    
    private func paintViewRenamed(newName: String?) {
        self.tableView.reloadData()
    }
    
    @IBAction func btShareClick(sender: UIButton) {
        self.shareSlidePickerController.action = share
        
        self.shareSlidePickerController.show(fromView: self.btShare, ofHeight: self.btShare.bounds.height, withViewController: self.viewController)
    }
    
    /**
     Action called when the user choose the action to execute
     
     - parameter action: Action to execute
     */
    func share(action action: ShareAction) {
        switch action {
        case .Send:
            Global_iOS.sharedInstance.commCenter?.send(oneSlide: Global_iOS.sharedInstance.documentController.currentDocument.getSlide(at: self.slideNumber)!)
        case .Copy:
            //The copy can take time so display a waiting window
            self.tableView.makeToastActivity(.Center)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                Global_iOS.sharedInstance.documentController.currentDocument.copySlide(at: self.slideNumber)
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                    
                    self.tableView.hideToastActivity()
                })
            })
        }
    }
}
