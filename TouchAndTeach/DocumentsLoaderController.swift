import UIKit

import SCLAlertView

/**
 The DocumentsLoaderController class is the view controller of the list of document in memory
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class DocumentsLoaderController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var onDocumentLoadedAction: Optional<() -> ()> = nil
    
    @IBOutlet weak var tableView: UITableView!
    
    var documents: [DocumentRepresentation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshDatas()
    }
    
    /**
     Refresh list of document from the document manager class
     */
    private func refreshDatas() {
        self.documents = DocumentsManager.sharedInstance.getDocumentsList
        self.tableView.reloadData()
    }
    
    @IBAction func btCancelClick(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.documents.count == 0 ? 1 : self.documents.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 133
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DocumentCell", forIndexPath: indexPath) as! DocumentCell
        
        cell.prepareCell(withDocumentRepresentation: self.documents.count == 0 ? nil : self.documents[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return self.documents.count > 0
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //If the user want to delete a document, ask for security if the user is sure of his manipulation
        if editingStyle == .Delete {
            let alertView = SCLAlertView()
            
            alertView.addButton(NSLocalizedString("YES", comment: "YES")) {
                do {
                    try DocumentsManager.sharedInstance.removeDocument(withName: self.documents[indexPath.row].name)
                    
                    self.view.makeToast(NSLocalizedString("DOCUMENT_DELETED", comment: "DOCUMENT_DELETED"))
                    
                    self.refreshDatas()
                } catch {}
            }
            
            alertView.showCustom(NSLocalizedString("DOCUMENT_DELETE_TITLE", comment: "DOCUMENT_DELETE_TITLE"), subTitle: String(format: NSLocalizedString("DOCUMENT_DELETE_MSG", comment: "DOCUMENT_DELETE_MSG"), self.documents[indexPath.row].name), color: Colors.primaryColor, icon: UIImage(named: "iconDelete")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.documents.count > 0 {
            self.view.makeToastActivity(.Center)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                //Load and change the current document (doing it asynchronously because it can take time)
                let loadedDocument = DocumentsManager.sharedInstance.loadDocument(withName: self.documents[indexPath.row].name)!
                
                dispatch_async(dispatch_get_main_queue(), {
                    Global_iOS.sharedInstance.documentController.currentDocument = loadedDocument
                    
                    self.view.hideToastActivity()
                    
                    if let onDocumentLoadedAction = self.onDocumentLoadedAction {
                        onDocumentLoadedAction()
                    }
                    
                    self.dismissViewControllerAnimated(true, completion: nil)
                })
            })
        }
    }
}