import Foundation
import UIKit

/**
 The MenuElement struct define the content of a menu item
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
struct MenuElement {
    var image: UIImage
    var title: String
    
    var action: () -> ()
    
    func executeAction() {
        self.action()
    }
}