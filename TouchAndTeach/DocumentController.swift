import Foundation
import UIKit

/**
 The DocumentController class is a abstraction for the document class for not lose the delegate when the document have to change
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class DocumentController {
    var currentDocument: Document = Document() {
        didSet {
            self.currentDocument.refresh(withDelegate: self.documentDelegate, withPaintViewDelegate: self.paintViewDelegate)
        }
    }
    
    /**
     This is the delegate which we have to save for a possible Document changement
     
     - note: When set it relay the delegate to the current document
     */
    var documentDelegate: DocumentDelegate? {
        willSet {
            self.currentDocument.delegate = newValue
        }
    }
    
    /**
     This is the delegate which we have to save for a possible Document changement
     
     - note: When set it relay the delegate to the current document
     */
    var paintViewDelegate: PaintViewDelegate? {
        willSet {
            self.currentDocument.paintViewDelegate = newValue
        }
    }
}