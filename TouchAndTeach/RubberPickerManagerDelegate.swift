import Foundation
import UIKit

/**
 The RubberPickerManagerDelegate protocol defines the methods called by the RubberPickerManager in response to important events like the rubber mode changement
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
protocol RubberPickerManagerDelegate {
    func rubberPicker(manager manager: RubberPickerManager, buttonClick newMode: RubberPickerManager.Mode)
}