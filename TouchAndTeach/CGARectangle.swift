import UIKit
import Foundation

/**
 The CGARectangle class describe a rectangle
 
 - note: For functions commentaries look at the CGAGeometry class commentaries
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGARectangle: NSObject, CGAGeometry, Serializable, Deserializable, NSCoding {
    var start: CGAPoint?
    var stop: CGAPoint?
    
    var Uuid: String = ""
    
    /**
     Represent the CGRect which equal to the Rectangle coordinates and surface
    */
    private var rectangle: CGRect? {
        get {
            if let stop = self.stop, let start = self.start {
                return CGRect(x: start.preciseLocation.x, y: start.preciseLocation.y, width: stop.preciseLocation.x - start.preciseLocation.x, height: stop.preciseLocation.y - start.preciseLocation.y)
            }
            
            return nil
        }
    }
    
    private(set) var painter:CGAPainter = CGAPainter()
    
    var minX: CGFloat {
        get {
            return self.rectangle!.minX
        }
    }
    var minY: CGFloat {
        get {
            return self.rectangle!.minY
        }
    }
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["Start", "Stop", "MatrixTransform", "ObjectType", "Uuid", "Painter"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "Start":
            return self.start
        case "Stop":
            return self.stop
        case "MatrixTransform":
            return "1.0,0.0,0.0,1.0,0.0,0.0"
        case "Uuid":
            return self.Uuid
        case "Painter":
            return self.painter
        case "ObjectType":
            return "Rectangle"
        default:
            return nil
        }
    }
    
    required init(fromJson json: AnyObject) {
        if let json = json as? NSDictionary {
            var matrixTransform = CGAffineTransformIdentity
            if let matrix = json["MatrixTransform"] as? String {
                let matrixTable = matrix.componentsSeparatedByString(",")
                
                if matrixTable.count == 6 {
                    matrixTransform = CGAffineTransform(a: CGFloat(Float(matrixTable[0])!), b: CGFloat(Float(matrixTable[1])!), c: CGFloat(Float(matrixTable[2])!), d: CGFloat(Float(matrixTable[3])!), tx: CGFloat(Float(matrixTable[4])!), ty: CGFloat(Float(matrixTable[5])!))
                }
            }
            
            if let startString = json["Start"] as? NSDictionary {
                self.start = CGAPoint(fromJson: startString)
                self.start!.apply(transformationMatrix: matrixTransform)
            }
            
            if let stopString = json["Stop"] as? NSDictionary {
                self.stop = CGAPoint(fromJson: stopString)
                self.stop!.apply(transformationMatrix: matrixTransform)
            }
            
            if let painter = json["Painter"] as? NSDictionary {
                self.painter = CGAPainter(fromJson: painter)
            }
            
            if let uuid = json["Uuid"] as? String {
                self.Uuid = uuid
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        self.start = aDecoder.decodeObjectForKey("start") as! CGAPoint?
        self.stop = aDecoder.decodeObjectForKey("stop") as! CGAPoint?
        self.Uuid = aDecoder.decodeObjectForKey("Uuid") as! String
        self.painter = aDecoder.decodeObjectForKey("painter") as! CGAPainter
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.start, forKey: "start")
        aCoder.encodeObject(self.stop, forKey: "stop")
        aCoder.encodeObject(self.Uuid, forKey: "Uuid")
        aCoder.encodeObject(self.painter, forKey: "painter")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(withPainter painter: CGAPainter) {
        super.init()
        
        self.generateUuid()
        
        self.painter = painter
    }
    
    func detected(touch touch: UITouch, withEvent event: UIEvent?) {
        if self.start != nil {
            self.stop = CGAPoint(fromTouch: touch)
        } else {
            self.start = CGAPoint(fromTouch: touch)
        }
    }
    
    func ended(touch touch: UITouch) {
        self.detected(touch: touch, withEvent: nil)
    }
    
    func draw(inContext context: CGContext) {
        if let rectangle = self.rectangle {
            CGContextSetStrokeColorWithColor(context, self.painter.color.CGColor)
            
            CGContextSetLineWidth(context, CGFloat(self.painter.thickness))
            
            CGContextStrokeRect(context, rectangle)
        }
    }
    
    /**
     Determine if a point is in the rectangle space
     
     - parameter point: Point to test
     
     - returns: Boolean that indicate if the point is in the rectangle
     */
    private func isInZone(forPosition point: CGPoint) -> Bool {
        if let rectangle = self.rectangle {
            return rectangle.contains(point)
        }
        
        return false
    }
    
    func isToErase(forTouch touch: UITouch) -> Bool {
        //We determinate that we have to delete with detect if the previous touch is out the rectangle and the actual il and reverse that signify that the user have reach the border of the rectangle
        if let _ = self.rectangle {
            let isPreviousTouchIn = self.isInZone(forPosition: touch.precisePreviousLocationInView(touch.view))
            let isActualTouchIn = self.isInZone(forPosition: touch.preciseLocationInView(touch.view))
            
            return isPreviousTouchIn == !isActualTouchIn
        }
        
        return false
    }
    
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        self.painter.thickness *= Float(gesture.scale)
        
        self.start?.pinchToZoom(fromGesture: gesture)
        self.stop?.pinchToZoom(fromGesture: gesture)
    }
    
    func move(x x: CGFloat, y: CGFloat) {
        self.start?.move(x: x, y: y)
        self.stop?.move(x: x, y: y)
    }
}