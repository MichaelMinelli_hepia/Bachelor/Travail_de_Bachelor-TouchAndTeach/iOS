import UIKit
import Foundation

/**
 The CGAStraightLine class describe a straight line
 
 - note: For functions commentaries look at the CGAGeometry class commentaries
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGAStraightLine: NSObject, CGAGeometry, Serializable, Deserializable, NSCoding {
    var start: CGAPoint?
    var stop: CGAPoint?
    
    var Uuid: String = ""
    
    private(set) var painter:CGAPainter = CGAPainter()
    
    var minX: CGFloat {
        get {
            var result = CGFloat.infinity
            
            if let start = self.start?.preciseLocation.x {
                result = start
            }
            
            if let stop = self.stop?.preciseLocation.x {
                if stop < result {
                    result = stop
                }
            }
            
            return result
        }
    }
    var minY: CGFloat {
        get {
            var result = CGFloat.infinity
            
            if let start = self.start?.preciseLocation.y {
                result = start
            }
            
            if let stop = self.stop?.preciseLocation.y {
                if stop < result {
                    result = stop
                }
            }
            
            return result
        }
    }
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["Start", "Stop", "MatrixTransform", "ObjectType", "Uuid", "Painter"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "Start":
            return start
        case "Stop":
            return stop
        case "MatrixTransform":
            return "1.0,0.0,0.0,1.0,0.0,0.0"
        case "Uuid":
            return Uuid
        case "Painter":
            return painter
        case "ObjectType":
            return "Line"
        default:
            return nil
        }
    }
    
    required init(fromJson json: AnyObject) {
        if let json = json as? NSDictionary {
            var matrixTransform = CGAffineTransformIdentity
            if let matrix = json["MatrixTransform"] as? String {
                let matrixTable = matrix.componentsSeparatedByString(",")
                
                if matrixTable.count == 6 {
                    matrixTransform = CGAffineTransform(a: CGFloat(Float(matrixTable[0])!), b: CGFloat(Float(matrixTable[1])!), c: CGFloat(Float(matrixTable[2])!), d: CGFloat(Float(matrixTable[3])!), tx: CGFloat(Float(matrixTable[4])!), ty: CGFloat(Float(matrixTable[5])!))
                }
            }
            
            if let startString = json["Start"] as? NSDictionary {
                self.start = CGAPoint(fromJson: startString)
                self.start!.apply(transformationMatrix: matrixTransform)
            }
            
            if let stopString = json["Stop"] as? NSDictionary {
                self.stop = CGAPoint(fromJson: stopString)
                self.stop!.apply(transformationMatrix: matrixTransform)
            }
            
            if let painter = json["Painter"] as? NSDictionary {
                self.painter = CGAPainter(fromJson: painter)
            }
            
            if let uuid = json["Uuid"] as? String {
                self.Uuid = uuid
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        self.start = aDecoder.decodeObjectForKey("start") as! CGAPoint?
        self.stop = aDecoder.decodeObjectForKey("stop") as! CGAPoint?
        self.Uuid = aDecoder.decodeObjectForKey("Uuid") as! String
        self.painter = aDecoder.decodeObjectForKey("painter") as! CGAPainter
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.start, forKey: "start")
        aCoder.encodeObject(self.stop, forKey: "stop")
        aCoder.encodeObject(self.Uuid, forKey: "Uuid")
        aCoder.encodeObject(self.painter, forKey: "painter")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(withPainter painter: CGAPainter) {
        super.init()
        
        self.generateUuid()
        
        self.painter = painter
    }
    
    func detected(touch touch: UITouch, withEvent event: UIEvent?) {
        if self.start != nil {
            self.stop = CGAPoint(fromTouch: touch)
        } else {
            self.start = CGAPoint(fromTouch: touch)
        }
    }
    
    func ended(touch touch: UITouch) {
        self.detected(touch: touch, withEvent: nil)
    }
    
    func draw(inContext context: CGContext) {
        if let start = self.start, let stop = self.stop {
            CGContextSetStrokeColorWithColor(context, self.painter.color.CGColor)
            
            CGContextBeginPath(context)
            
            //Draw the line from the start point (first touch) to the last point (last touch)
            CGContextMoveToPoint(context, start.preciseLocation.x, start.preciseLocation.y)
            CGContextAddLineToPoint(context, stop.preciseLocation.x, stop.preciseLocation.y)

            CGContextSetLineWidth(context, CGFloat(self.painter.thickness))
            CGContextStrokePath(context)
        }
    }
    
    func isToErase(forTouch touch: UITouch) -> Bool {
        if let start = self.start, let stop = self.stop {
            return self.isIntersectionExist(betweenTouch: touch, andPoint: start, andPoint: stop)
        }
    
        return false
    }
    
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        self.painter.thickness *= Float(gesture.scale)
        
        self.start?.pinchToZoom(fromGesture: gesture)
        self.stop?.pinchToZoom(fromGesture: gesture)
    }
    
    func move(x x: CGFloat, y: CGFloat) {
        self.start?.move(x: x, y: y)
        self.stop?.move(x: x, y: y)
    }
}