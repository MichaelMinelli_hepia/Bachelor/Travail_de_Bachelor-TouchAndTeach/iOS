import UIKit

/**
 The MenuCell tableview cell is the cell of the left menu
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class MenuCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblTitle.textColor = Colors.menuSecondaryColor
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /**
     Prepare cell for show a new content
     
     - parameter menuElement: Element to show
     */
    func prepareCell(withMenuElement menuElement: MenuElement) {
        self.backgroundColor = UIColor.clearColor()
        
        self.img.image = menuElement.image
        self.lblTitle.text = menuElement.title
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        self.backgroundColor = highlighted ? Colors.menuTableViewSecondaryColor : UIColor.clearColor()
    }
}
