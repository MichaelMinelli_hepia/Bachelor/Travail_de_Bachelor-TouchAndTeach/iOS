import UIKit

/**
 The PhotoController class is the view controller of the choice of method for import photo
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class PhotoController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.preferredContentSize = CGSizeMake(300, 220);
    }
    
    @IBAction func btTakePictureClick(sender: UIButton) {
        self.loadPicture(from: .Camera)
    }
    
    @IBAction func btLibrary(sender: UIButton) {
        self.loadPicture(from: .PhotoLibrary)
    }
    
    @IBAction func btCancelClick(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /**
     Show the UIImage Picket Controller
     
     - parameter source: Method (UIImagePickerControllerSourceType) of selection of the image
     */
    private func loadPicture(from source: UIImagePickerControllerSourceType) {
        self.imagePicker =  UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = source
        presentViewController(self.imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //Send the image
            Global_iOS.sharedInstance.commCenter?.send(image: image.rotatedCopy)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}