import UIKit
import Foundation

import SCLAlertView

/**
 The PaintView class is a draw view
 
 - note: It's represent a slide
 - author: Michaël Minelli
 - version: 1.0.0
 */
class PaintView: UIView, Serializable, Deserializable {
    static let PREFERED_MARGIN = 25
    
    /**
     The TouchMode enum represent the draw mode the user want to use
     
     - Draw: For drawing form
     - Rubber: For erase part of the paint view
     - Resize: For resize or move the paint view
     */
    enum TouchMode: Int {
        case Draw
        case Rubber
        case Resize
    }
    
    var name: String? {
        didSet {
            //Notify the delegate that the name have change
            self.delegate?.paintView(self, changeName: self.name)
        }
    }
    
    var delegate: PaintViewDelegate?
    
    private static let DEFAULT_COLOR: UIColor = UIColor.blackColor()
    private static let DEFAULT_THICKNESS: Float = 3.0
    
    var controller: UIViewController = UIViewController()
    
    private var geometricObjects = [CGAGeometry]() //Contain all objects which are finish to be drawing
    private let activeGeometricObjects = NSMapTable.strongToStrongObjectsMapTable() //Contain objects which is in drawing
    
    //Stack for undo and redo function
    private var previousGeometricObjects = Stack<[CGAGeometry]>()
    private var nextGeometricObjects = Stack<[CGAGeometry]>()
    
    //These variables are used to generate a image of the view for not have to redraw all element each time (Big plus for performance)
    private var frozenContext: CGContext? //Context where we draw all finished object and we take a picture of that
    private var frozenImage: CGImage?
    var needUpdateFrozenImage: Bool = true
    
    /**
     Return an image (if possible) in the UIImage format
    */
    var image: UIImage? {
        get {
            return self.frozenImage == nil ? nil : UIImage(CGImage: self.frozenImage!, scale: 1.0, orientation: .DownMirrored)
        }
    }
    
    //Recognizer of pinch gesture for zomm event
    private var pinchGesture: UIPinchGestureRecognizer?
    
    //Variables which defines next drawing method and property is use with the next touch event
    var nextGeometricObjectColor:UIColor = PaintView.DEFAULT_COLOR
    var nextGeometricObjectThickness: Float = PaintView.DEFAULT_THICKNESS
    var nextGeometricObjectTouchMode: TouchMode = .Draw {
        willSet {
            //If the new mode is not the drawing mode, the multi-touch is disabled
            self.multipleTouchEnabled = newValue != .Rubber
            
            //If the new mode is for move and resize, enable the pinch gesture otherwise disable it
            if newValue == .Resize {
                if pinchGesture == nil {
                    pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(PaintView.recognizePinchGesture(_:)))
                }
                
                self.addGestureRecognizer(pinchGesture!)
            } else {
                if let pinchGesture = pinchGesture {
                    self.removeGestureRecognizer(pinchGesture)
                }
            }
        }
    }
    var nextDrawType: CGAGeometryForm = .Line
    
    //Coordinate of the current point in rubber mode, it's used for show the circle around the touch position
    private var pointOfRubber: CGPoint?
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["AllDrawObject", "Background", "Title"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "AllDrawObject":
            return self.geometricObjects
        case "Background":
            return nil
        case "Title":
            return self.name
        default:
            return ""
        }
    }
    
    /**
     Init the slide with a json element
     
     - parameter json: The json dictionnary
     */
    convenience required init(fromJson json: AnyObject) {
        self.init(frame: CGRectZero)
        
        var drawToAdd: [CGAGeometry] = []
        
        //Min values are defines for replace the draw in the visible screen
        var minX: CGFloat = CGFloat.infinity
        var minY: CGFloat = CGFloat.infinity
        
        if let json = json as? NSDictionary {
            if let title = json["Title"] as? String {
                self.name = title
            }
            
            //Get all drawobject from the json
            if let allDrawObject = json["AllDrawObject"] as? NSArray {
                for drawObject in allDrawObject {
                    if let drawObject = drawObject as? NSDictionary {
                        //Ask the factory for the draw
                        if let draw = CGAGeometryFactory.getGeometricObject(fromJson: drawObject) {
                            drawToAdd.append(draw)
                            
                            minX = min(draw.minX, minX)
                            minY = min(draw.minY, minY)
                        }
                    }
                }
            }
        }
        
        //Replace the draw in the top left of the screen
        for draw in drawToAdd {
            draw.move(x: -minX + CGFloat(PaintView.PREFERED_MARGIN), y: -minY + CGFloat(PaintView.PREFERED_MARGIN))
            
            self.add(geometricObject: draw)
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.name = aDecoder.decodeObjectForKey("name") as? String
        self.geometricObjects = aDecoder.decodeObjectForKey("geometricObjects") as! [CGAGeometry]
        if let image = aDecoder.decodeObjectForKey("frozenImage") as? UIImage {
            let ciImage = CIImage(image: image)!
            
            let context = CIContext(options: nil)
            self.frozenImage = context.createCGImage(ciImage, fromRect: ciImage.extent)
        }
        self.nextGeometricObjectColor = aDecoder.decodeObjectForKey("nextLineColor") as! UIColor
        self.nextGeometricObjectThickness = aDecoder.decodeFloatForKey("nextLineThickness")
        self.nextGeometricObjectTouchMode = TouchMode(rawValue: aDecoder.decodeIntegerForKey("nextLineTouchMode"))!
        self.nextDrawType = CGAGeometryForm(rawValue: aDecoder.decodeIntegerForKey("nextDrawType"))!
        
        self.setNeedsDisplayWithFrozenImageUpdated()
    }
    
    override func encodeWithCoder(aCoder: NSCoder) {
        super.encodeWithCoder(aCoder)
        
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeObject(self.geometricObjects, forKey: "geometricObjects")
        if let frozenImage = self.frozenImage {
            aCoder.encodeObject(UIImage(CGImage: frozenImage), forKey: "frozenImage")
        }
        aCoder.encodeObject(self.nextGeometricObjectColor, forKey: "nextLineColor")
        aCoder.encodeFloat(self.nextGeometricObjectThickness, forKey: "nextLineThickness")
        aCoder.encodeInteger(self.nextGeometricObjectTouchMode.rawValue, forKey: "nextLineTouchMode")
        aCoder.encodeInteger(self.nextDrawType.rawValue, forKey: "nextDrawType")
        
        setNeedsDisplayWithFrozenImageUpdated()
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
        self.multipleTouchEnabled = true
    }
    
    /**
     Change the name of the slide and possibily (optional) notify when changement have been done
     
     - parameter action: Handler for rename completion
     */
    func rename(withCompletionHandler action: Optional<(_:String?) -> ()> = nil) {
        //Alert view for ask the user the new name
        let alert = SCLAlertView()
        let txt = alert.addTextField(NSLocalizedString("PAINTVIEW_ENTER_NAME", comment: "PAINTVIEW_ENTER_NAME"))
        txt.text = self.name
        alert.addButton(NSLocalizedString("PAINTVIEW_ENTER_NAME_BUTTON", comment: "PAINTVIEW_ENTER_NAME_BUTTON")) {
            if let text = txt.text {
                self.name = text
                
                if let action = action {
                    action(text)
                }
            } else {
                if let action = action {
                    action(nil)
                }
            }
        }
        alert.showCustom(NSLocalizedString("PAINTVIEW_ENTER_NAME_TITLE", comment: "PAINTVIEW_ENTER_NAME_TITLE"), subTitle: NSLocalizedString("PAINTVIEW_ENTER_NAME_MSG", comment: "PAINTVIEW_ENTER_NAME_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconName")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
    }
    
    //////////////////////////////////////////////////////////// Touch detection //////////////////////////////////////////////////////////////
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.detected(touches: touches, withEvent: event)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.detected(touches: touches, withEvent: event)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.ended(touches: touches)
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        self.canceled(touches: touches)
    }
    
    override func touchesEstimatedPropertiesUpdated(touches: Set<NSObject>) {
        
    }
    
    /**
     Function called when a pinch gesture is recognize by the system
     
     - parameter sender: The pinch gesture object
     */
    func recognizePinchGesture(sender: UIPinchGestureRecognizer)
    {
        self.pinchToZoom(fromGesture: sender)
        self.setNeedsDisplayWithFrozenImageUpdated()
        
        sender.scale = 1 //Must reinit this value
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     setNeedsDisplay with indication that we need to update the frozenImage
     */
    func setNeedsDisplayWithFrozenImageUpdated() {
        self.needUpdateFrozenImage = true
        self.setNeedsDisplay()
    }
    
    /**
     This function update the frozen image by call to the redraw of all finished object and take picture of the view
     */
    func updateFrozenImage(withWindow window: UIWindow?) {
        if let window = window {
            //Recreate the frozen context (context where we draw all finished object)
            let scale = window.screen.scale
            var size = self.bounds.size
            
            size.width *= scale
            size.height *= scale
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            
            self.frozenContext = CGBitmapContextCreate(nil, Int(size.width), Int(size.height), 8, 0, colorSpace, CGImageAlphaInfo.PremultipliedLast.rawValue)
            
            CGContextSetLineCap(self.frozenContext, .Round)
            let transform = CGAffineTransformMakeScale(scale, scale)
            CGContextConcatCTM(self.frozenContext, transform)
            
            CGContextClearRect(self.frozenContext, self.bounds)
            
            //Draw all finished object
            for geometricObject in self.geometricObjects {
                geometricObject.draw(inContext: self.frozenContext!)
            }
            
            //Generation of the frozen image
            self.frozenImage = CGBitmapContextCreateImage(self.frozenContext)
        }
    }
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()!
        
        CGContextSetLineCap(context, CGLineCap.Round)
        
        //If needed, regenerate the frozen image
        if self.needUpdateFrozenImage {
            self.updateFrozenImage(withWindow: self.window)
            
            self.needUpdateFrozenImage = false
        }
        
        //Show the frozen image (all finished draw object)
        if let frozenImage = self.frozenImage {
            CGContextDrawImage(context, bounds, frozenImage)
        }

        //Draw all active (in draw) draw objects
        let enumerator = activeGeometricObjects.objectEnumerator()
        while let geometricObject: AnyObject = enumerator!.nextObject() {
            geometricObject.drawInContext(context)
        }
        
        //If we have a rubber to show (a circle around the touch position)
        if let pointOfRubber = self.pointOfRubber {
            CGContextSetFillColorWithColor(context, UIColor(white: 0.5, alpha: 0.3).CGColor)
            
            CGContextSetLineWidth(context, CGFloat(3.0))
            
            CGContextAddEllipseInRect(context, CGRect(x: pointOfRubber.x - 15, y: pointOfRubber.y - 15, width: 30, height: 30))
            
            CGContextFillPath(context)
        }
    }
    
    /**
     Function to call when a touch is detected
     
     - parameter touches: All touches detected
     - parameter event:   Optional Event
     */
    private func detected(touches touches: Set<UITouch>, withEvent event: UIEvent?) {
        //For each detected touches
        for touch in touches {
            //Verify that we can't bad interpret a touch which a not in the currect view
            if let view = touch.view where view == self || view is UITextView {
                //Search the action to do by test the touch mode
                switch nextGeometricObjectTouchMode {
                case .Draw:
                    // Retrieve a draw object from `activeGeometricObjects`. If no line exists (when it's a first touch), create one.
                    let geometricObject = activeGeometricObjects.objectForKey(touch) as? CGAGeometry ?? addActiveGeometricObject(forTouch: touch)
                    
                    geometricObject.detected(touch: touch, withEvent: event)
                case .Rubber:
                    self.erase(forTouch: touch)
                case .Resize:
                    self.move(forTouch: touch)
                }
            }
        }
        
        self.setNeedsDisplay()
    }
    
    /**
     Rubber function which verify where the touch is and test object to look if delete some object is needed+
     
     - parameter touch: The touch
     */
    private func erase(forTouch touch: UITouch) {
        //Take the position
        self.pointOfRubber = touch.preciseLocationInView(touch.view)
        
        var index = 0
        //Test all geometric object to now if we have to erase it
        for geometricObject in self.geometricObjects {
            //Ask the geometric object if the touch is in and erase the object if yes
            if geometricObject.isToErase(forTouch: touch) {
                self.nextGeometricObjects.removeAllObjects()
                self.previousGeometricObjects.push(self.geometricObjects)
                
                self.geometricObjects[index].erase?()
                self.geometricObjects.removeAtIndex(index)
                
                self.needUpdateFrozenImage = true
            } else {
                index += 1
            }
        }
    }
    
    /**
     Called when the touch mode is Resize for move the draw
     
     - parameter touch: The touch object
     */
    private func move(forTouch touch: UITouch) {
        let location = touch.preciseLocationInView(touch.view)
        let previousLocation = touch.precisePreviousLocationInView(touch.view)
        
        //Calculation of the derivation of the touch between the previous touch and the actual
        let moveX = location.x - previousLocation.x
        let moveY = location.y - previousLocation.y
        
        //Move each object
        for geometricObject in self.geometricObjects {
            geometricObject.move(x: CGFloat(moveX), y: CGFloat(moveY))
        }
        
        self.needUpdateFrozenImage = true
    }
    
    /**
     Called when the touch mode is Resize and a pinch gesture have been recognize
     
     - parameter gesture: Pinch gesture object
     */
    private func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        //Ask all draw object to resize them
        for geometricObject in self.geometricObjects {
            geometricObject.pinchToZoom(fromGesture: gesture)
        }
    }
    
    /**
     This function add a geometric object to the list
     
     - note: This function is to call when a first touch is detected
     
     - parameter touch: The touch object
     
     - returns: The object created and added to the list
     */
    private func addActiveGeometricObject(forTouch touch: UITouch) -> CGAGeometry {
        //Ask the creation of the object by the Factory (Design pattern Factory)
        let newGeometricObject: CGAGeometry = CGAGeometryFactory.getGeometricObject(ofType: self.nextDrawType, withPainter: CGAPainter(color: self.nextGeometricObjectColor, thickness: self.nextGeometricObjectThickness), forPaintView: self, withTouch: touch)
        self.activeGeometricObjects.setObject(newGeometricObject, forKey: touch)
        self.nextGeometricObjects.removeAllObjects()
        
        return newGeometricObject
    }
    
    /**
     Add a draw to the slide
     
     - parameter geometricObject: Geometric Object to add
     */
    func add(geometricObject geometricObject: CGAGeometry) {
        self.push(geometricObject: geometricObject)
        
        self.setNeedsDisplayWithFrozenImageUpdated()
    }
    
    /**
     Push a new geometric object to the list
     
     - parameter geometricObject: Geometric Object to add
     */
    private func push(geometricObject geometricObject: CGAGeometry) {
        //Update previous list
        self.previousGeometricObjects.push(self.geometricObjects)
        
        //Push the draw object
        self.geometricObjects.append(geometricObject)
    }
    
    /**
     Function called when touches is last touches (touch ended)
     
     - parameter touches: Touches objects
     */
    private func ended(touches touches: Set<UITouch>?) {
        guard let touches = touches else { return }
        
        for touch in touches {
            self.pointOfRubber = nil
            
            //Skip over touches that do not correspond to an active line.
            guard let geometricObject = self.activeGeometricObjects.objectForKey(touch) as? CGAGeometry else { continue }
            
            geometricObject.ended(touch: touch)
            
            //Add the state of objects in memory (for undo) then add object to the finished object
            self.push(geometricObject: geometricObject)
            self.activeGeometricObjects.removeObjectForKey(touch)
            
            //Add the draw object to the context and retake picture of this
            if let frozenContext = self.frozenContext {
                geometricObject.draw(inContext: frozenContext)
                self.frozenImage = CGBitmapContextCreateImage(frozenContext)
            }
        }
        
        self.setNeedsDisplay()
    }
    
    /**
     Function called when touches is canceled for remove the draw object
     
     - parameter touches: Touches objects
     */
    private func canceled(touches touches: Set<UITouch>?) {
        guard let touches = touches else { return }
        
        for touch in touches {
            self.activeGeometricObjects.removeObjectForKey(touch)
        }
        
        self.setNeedsDisplay()
    }
    
    /**
     Undo function restore the previous state of the view
     */
    func undo() {
        if let geometricObjects = self.previousGeometricObjects.pop() {
            self.nextGeometricObjects.push(self.geometricObjects)
            self.geometricObjects = geometricObjects
            
            
            self.setNeedsDisplayWithFrozenImageUpdated()
        }
    }
    
    /**
     Redo function recreate changes that have been undo
     */
    func redo() {
        if let geometricObjects = self.nextGeometricObjects.pop() {
            self.previousGeometricObjects.push(self.geometricObjects)
            self.geometricObjects = geometricObjects
            
            
            self.setNeedsDisplayWithFrozenImageUpdated()
        }
    }
    
    /**
     Clear the view from all draw objects
     */
    func clear() {
        self.geometricObjects.removeAll()
        self.activeGeometricObjects.removeAllObjects()
        self.nextGeometricObjects.removeAllObjects()
        
        self.setNeedsDisplayWithFrozenImageUpdated()
    }
}