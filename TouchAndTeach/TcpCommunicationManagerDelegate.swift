import Foundation

/**
 The TcpCommunicationManagerDelegate protocol defines the methods called by the TcpCommunicationManager in response to important events in the TCP socket communication
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
protocol TcpCommunicationManagerDelegate {
    func tcpCommunication(manager manager: TcpCommunicationManager, isConnectedToHost host: ConnectionInformations)
    func tcpCommunication(manager manager: TcpCommunicationManager, isDisconnectedFromHost host: ConnectionInformations)
    func tcpCommunication(manager manager: TcpCommunicationManager, receiveMessage data: NSData)
    func tcpCommunication(manager manager: TcpCommunicationManager, errorOccurred error: TcpCommunicationManager.Error)
}