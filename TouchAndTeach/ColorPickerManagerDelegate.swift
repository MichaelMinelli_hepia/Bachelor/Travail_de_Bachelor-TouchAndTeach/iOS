import Foundation
import UIKit

/**
 Description
 ---
 Show a color and slider picker
 
 Changements from original code
 ---
 * Add documentation
 * Add a delegate to send event (for example: the color changement)
 * Add a slider to add the thickness chooser
 * Modify the color change for change the color of the start of the view too
 * Bug fix (Array out of bound sometime append, the get color method was replaced by a calculation which can't finish out of the array)
 
 - authors: Michaël Minelli, Ethan Strider
 - seealso: [Original code](https://github.com/EthanStrider/ColorPickerExample)
 - version: 2.0.0
 */
protocol ColorPickerManagerDelegate {
    func colorPicker(manager manager: ColorPickerManager, selectNewColor color: UIColor)
    func colorPicker(manager manager: ColorPickerManager, selectNewThickness thickness: Float)
}