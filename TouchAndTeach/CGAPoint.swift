import Foundation
import UIKit
import CoreGraphics

/**
 Description
 ---
 The CGAPoint class represent a simplified and augmented UITouch class
 
 Addition to UITouch
 ---
 * JSON serializable
 * Pinch to zoom method
 * Move method
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGAPoint: NSObject, Serializable, Deserializable, NSCoding {
    private(set) var preciseLocation: CGPoint = CGPoint()
    
    //Not really necessary but saved for a possible futur usage
    let timestamp: NSTimeInterval
    private(set) var force: CGFloat = 0.0
    let type: UITouchType
    private(set) var altitudeAngle: CGFloat = 0.0
    private(set) var azimuthAngle: CGFloat = 0.0
    private(set) var estimatedPropertiesExpectingUpdates: UITouchProperties = UITouchProperties()
    private(set) var estimatedProperties: UITouchProperties = UITouchProperties()
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["X", "Y"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "X":
            return self.preciseLocation.x
        case "Y":
            return self.preciseLocation.y
        default:
            return ""
        }
    }
    
    required init(fromJson json: AnyObject) {
        if let json = json as? NSDictionary {
            if let x = json["X"] as? Float, let y = json["Y"] as? Float {
                self.preciseLocation = CGPoint(x: CGFloat(x), y: CGFloat(y))
            }
        }
        
        self.timestamp = NSDate.timeIntervalSinceReferenceDate()
        self.type = .Direct
    }
    
    required init(coder aDecoder: NSCoder) {
        self.preciseLocation = CGPoint(x: CGFloat(aDecoder.decodeFloatForKey("x")), y: CGFloat(aDecoder.decodeFloatForKey("y")))
        self.timestamp = aDecoder.decodeObjectForKey("timestamp") as! NSTimeInterval
        self.force = CGFloat(aDecoder.decodeFloatForKey("force"))
        self.type = UITouchType(rawValue: aDecoder.decodeIntegerForKey("type"))!
        self.altitudeAngle = CGFloat(aDecoder.decodeFloatForKey("altitudeAngle"))
        self.azimuthAngle = CGFloat(aDecoder.decodeFloatForKey("azimuthAngle"))
        self.estimatedPropertiesExpectingUpdates = UITouchProperties(rawValue: aDecoder.decodeIntegerForKey("estimatedPropertiesExpectingUpdates"))
        self.estimatedProperties = UITouchProperties(rawValue: aDecoder.decodeIntegerForKey("estimatedProperties"))
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeFloat(Float(self.preciseLocation.x), forKey: "x")
        aCoder.encodeFloat(Float(self.preciseLocation.y), forKey: "y")
        aCoder.encodeObject(self.timestamp, forKey: "timestamp")
        aCoder.encodeFloat(Float(self.force), forKey: "force")
        aCoder.encodeInteger(self.type.rawValue, forKey: "type")
        aCoder.encodeFloat(Float(self.altitudeAngle), forKey: "altitudeAngle")
        aCoder.encodeFloat(Float(self.azimuthAngle), forKey: "azimuthAngle")
        aCoder.encodeInteger(self.estimatedPropertiesExpectingUpdates.rawValue, forKey: "estimatedPropertiesExpectingUpdates")
        aCoder.encodeInteger(self.estimatedProperties.rawValue, forKey: "estimatedProperties")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(fromTouch touch: UITouch) {
        let view = touch.view
        self.timestamp = touch.timestamp
        self.preciseLocation = touch.preciseLocationInView(view)
        self.azimuthAngle = touch.azimuthAngleInView(view)
        self.estimatedProperties = touch.estimatedProperties
        self.estimatedPropertiesExpectingUpdates = touch.estimatedPropertiesExpectingUpdates
        self.altitudeAngle = touch.altitudeAngle
        self.type = touch.type
        self.force = (self.type == .Stylus || touch.force > 0) ? touch.force : 1.0
    }
    
    /**
     Apply a tranformation matrix to the location of the point
     
     - parameter matrix: Matrix to apply
     */
    func apply(transformationMatrix matrix: CGAffineTransform) {
        self.preciseLocation = CGPointApplyAffineTransform(self.preciseLocation, matrix)
    }
    
    /**
     Update coordinate of the point from the pinch to zoom gesture
     
     - seealso: [Method source](http://stackoverflow.com/questions/10532113/zooming-using-pinch-but-not-using-transform-methods)
     
     - parameter gesture: The pinch gesture object
     */
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        self.preciseLocation.x = (self.preciseLocation.x - gesture.locationInView(gesture.view!).x) * gesture.scale + gesture.locationInView(gesture.view!).x;
        self.preciseLocation.y = (self.preciseLocation.y - gesture.locationInView(gesture.view!).y) * gesture.scale + gesture.locationInView(gesture.view!).y;
    }
    
    /**
     Move the point by the specified amount
     
     - parameter x: X derivation
     - parameter y: Y derivation
     */
    func move(x x: CGFloat, y: CGFloat) {
        self.preciseLocation.x += x
        self.preciseLocation.y += y
    }
}