import UIKit

/**
 The DocumentCell tableview cell is the cell of display a document in the tableview of stored document
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class DocumentCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /**
     Prepare cell for show a new content
     
     - parameter documentRepresentation: Element to show
     */
    func prepareCell(withDocumentRepresentation documentRepresentation: DocumentRepresentation?) {
        //If the argument is nil it's there is no document in the storage
        if let documentRepresentation = documentRepresentation {
            self.img.image = documentRepresentation.image
            self.lblName.text = documentRepresentation.name
            
            //Show the last saved date
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "EEEE dd MMMM YYYY  HH:mm"
            self.lblDate.text = dateFormatter.stringFromDate(documentRepresentation.lastSave).capitalizedString
        } else {
            self.img.image = nil
            self.lblName.text = NSLocalizedString("DOCUMENT_NOT_FOUND", comment: "DOCUMENT_NOT_FOUND")
            self.lblDate.text = ""
        }
    }
}
