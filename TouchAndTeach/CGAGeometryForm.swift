import Foundation

/**
 The CGAGeometryForm enum contains all draw object types
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
enum CGAGeometryForm: Int {
    case Line
    case Rectangle
    case Ellipse
    case StraightLine
    case Text
}