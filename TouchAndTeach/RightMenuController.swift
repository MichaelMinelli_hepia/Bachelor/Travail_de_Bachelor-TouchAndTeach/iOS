import Foundation
import UIKit

/**
 The MenuController class is the view controller of right menu.
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class RightMenuController: MenuController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(RightMenuController.longPressGestureRecognized(withGesture:)))
        self.tableView.addGestureRecognizer(longpress)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global_iOS.sharedInstance.documentController.currentDocument.count + 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 215
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(indexPath.row == Global_iOS.sharedInstance.documentController.currentDocument.count ? "SlideCellAdd" : "SlideCell", forIndexPath: indexPath) as! SlideCell
        
        cell.prepareCell(withSlideNumber: indexPath.row, forTableView: self.tableView, onViewController: self)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //Verify if the user want to load a slide or create a new document
        if indexPath.row != Global_iOS.sharedInstance.documentController.currentDocument.count {
            Global_iOS.sharedInstance.documentController.currentDocument.changeCurrentSlide(toNewSlideIndex: indexPath.row)
        } else {
            Global_iOS.sharedInstance.documentController.currentDocument.createSlide(addItToSlidesList: true, setItLikeCurrentSlide: true)
        }
        
        self.tableView.reloadData()
        
        self.slideMenuController()?.closeRight()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    /**
     Function called by a long press gesture. This function is used for moved slides a reorder it.
     
     - seealso: [Original code : DragNDrop](https://github.com/fairbanksdan/DragNDrop-Final)
     - note: Some modifications were done for adapt to the need of this application
     */
    func longPressGestureRecognized(withGesture gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.locationInView(self.tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(locationInView)
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        struct Path {
            static var initialIndexPath : NSIndexPath? = nil
        }
        
        //Verify the state of the gesture
        switch state {
        case UIGestureRecognizerState.Began:
            //Verify that the user don't try to move the add slide button
            if let indexPath = indexPath where indexPath.row != Global_iOS.sharedInstance.documentController.currentDocument.count {
                Path.initialIndexPath = indexPath
                let cell = self.tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!
                My.cellSnapshot  = self.snapshotOfCell(withView: cell)
                
                var center = cell.center
                My.cellSnapshot!.center = center
                My.cellSnapshot!.alpha = 0.0
                self.tableView.addSubview(My.cellSnapshot!)
                
                //Animation for display the transparent image (view)
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    My.cellIsAnimating = true
                    My.cellSnapshot!.center = center
                    My.cellSnapshot!.transform = CGAffineTransformMakeScale(1.05, 1.05)
                    My.cellSnapshot!.alpha = 0.98
                    cell.alpha = 0.0
                    }, completion: { (finished) -> Void in
                        if finished {
                            My.cellIsAnimating = false
                            if My.cellNeedToShow {
                                My.cellNeedToShow = false
                                UIView.animateWithDuration(0.25, animations: { () -> Void in
                                    cell.alpha = 1
                                })
                            } else {
                                cell.hidden = true
                            }
                        }
                })
            }
            
        case UIGestureRecognizerState.Changed:
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                center.y = locationInView.y
                My.cellSnapshot!.center = center
                
                //Detect if the movement have reach a slide position and move it
                if let indexPath = indexPath where indexPath != Path.initialIndexPath && indexPath.row != Global_iOS.sharedInstance.documentController.currentDocument.count {
                    Global_iOS.sharedInstance.documentController.currentDocument.moveSlide(from: Path.initialIndexPath!.row, to:indexPath.row)
                    self.tableView.moveRowAtIndexPath(Path.initialIndexPath!, toIndexPath: indexPath)
                    Path.initialIndexPath = indexPath
                }
            }
        default:
            if Path.initialIndexPath != nil {
                let cell = self.tableView.cellForRowAtIndexPath(Path.initialIndexPath!) as UITableViewCell!
                if My.cellIsAnimating {
                    My.cellNeedToShow = true
                } else {
                    cell.hidden = false
                    cell.alpha = 0.0
                }
                
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    My.cellSnapshot!.center = cell.center
                    My.cellSnapshot!.transform = CGAffineTransformIdentity
                    My.cellSnapshot!.alpha = 0.0
                    cell.alpha = 1.0
                    
                    }, completion: { (finished) -> Void in
                        if finished {
                            Path.initialIndexPath = nil
                            My.cellSnapshot!.removeFromSuperview()
                            My.cellSnapshot = nil
                        }
                })
            }
            
            if state == UIGestureRecognizerState.Ended {
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     Create a semi transparent image from a view
     
     - seealso: [Original code : DragNDrop](https://github.com/fairbanksdan/DragNDrop-Final)
     - note: Some modifications were done for adapt to the need of this application
     */
    func snapshotOfCell(withView inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
}