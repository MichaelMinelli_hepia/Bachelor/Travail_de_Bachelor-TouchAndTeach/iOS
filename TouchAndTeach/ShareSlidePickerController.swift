import UIKit

/**
 The ShareSlidePickerController class show a menu for select the draw mode
 
 - authors: Michaël Minelli
 - version: 1.0.0
 */
class ShareSlidePickerController: UITableViewController {
    private static let WIDTH  = CGFloat(300)
    private static let HEIGHT = CGFloat(87)
    
    //Function to be called when the user choose an action
    var action: Optional<(_:SlideCell.ShareAction) -> ()> = nil
    
    /**
     Show the draw picker
     
     - parameter sourceView: View which call this function (generally a button)
     - parameter height:     Height of the source view
     - parameter controller: The current view controller
     */
    func show(fromView sourceView: UIView, ofHeight height: CGFloat, withViewController controller: UIViewController) {
        self.modalPresentationStyle = .Popover
        self.preferredContentSize = CGSizeMake(ShareSlidePickerController.WIDTH, ShareSlidePickerController.HEIGHT)
        
        if let popoverController = self.popoverPresentationController {
            popoverController.sourceView = sourceView
            popoverController.sourceRect = CGRect(x: -1, y: 0, width: 0, height: height)
            popoverController.permittedArrowDirections = .Right
            popoverController.backgroundColor = UIColor.whiteColor()
            popoverController.delegate = nil
        }
        
        controller.presentViewController(self, animated: true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let action = self.action {
            //Execute the action needed by the user choice
            action(SlideCell.ShareAction(rawValue: indexPath.row)!)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}