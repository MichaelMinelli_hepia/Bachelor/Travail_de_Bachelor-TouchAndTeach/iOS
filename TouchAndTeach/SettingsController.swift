import UIKit

/**
 The SettingsController class is the view controller of the settings view
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class SettingsController: UIViewController, UITextFieldDelegate, CommCenterDelegate {
    
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var txtUserName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initGlobalAppearance()
        Global_iOS.sharedInstance.commCenter?.delegate = self
        
        self.lblVersion.text = (NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as? String)!
        
        self.txtUserName.text = Preferences.sharedInstance.getUsername()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem(withName: NSLocalizedString("MENU_ITEM_SETTINGS", comment: "MENU_ITEM_SETTINGS"), isConnected: Global_iOS.sharedInstance.commCenter?.state == .Connected)
    }
    
    func commCenter(commCenter commCenter: CommCenter, connectionStateChange connected: Bool) {
        self.setNavigationBarItem(withName: NSLocalizedString("MENU_ITEM_SETTINGS", comment: "MENU_ITEM_SETTINGS"), isConnected: connected)
    }
    
    func commCenter(commCenter commCenter: CommCenter, receiveDocument document: Document) {
        Global_iOS.sharedInstance.onReceive(document: document, onView: self.view)
    }
    
    @IBAction func btNciLabClick(sender: UIButton) {
        //UIApplication.sharedApplication().openURL(NSURL(string:NSLocalizedString("NCI_LAB_LINK", comment: "NCI_LAB_LINK"))!)
    }
    
    @IBAction func txtUserNameEditingDidEnd(sender: UITextField) {
        Preferences.sharedInstance.set(username: sender.text!)
        
        Global_iOS.sharedInstance.commCenter?.send(userName: sender.text!)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}