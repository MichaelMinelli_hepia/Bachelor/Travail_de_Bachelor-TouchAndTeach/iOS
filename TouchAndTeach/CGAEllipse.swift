import UIKit
import Foundation

/**
 The CGAEllipse class describe a ellipse
 
 - note: For functions commentaries look at the CGAGeometry class commentaries
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGAEllipse: NSObject, CGAGeometry, Serializable, Deserializable, NSCoding {
    var start: CGAPoint?
    var stop: CGAPoint?
    
    var Uuid: String = ""
    
    /**
     Represent the CGRect which equal to the Rectangle coordinates and surface wich include the ellipse
     */
    private var rectangle: CGRect? {
        get {
            if let stop = self.stop, let start = self.start {
                return CGRect(x: start.preciseLocation.x, y: start.preciseLocation.y, width: stop.preciseLocation.x - start.preciseLocation.x, height: stop.preciseLocation.y - start.preciseLocation.y)
            }
            
            return nil
        }
    }
    
    private(set) var painter:CGAPainter = CGAPainter()
    
    var minX: CGFloat {
        get {
            return self.rectangle!.minX
        }
    }
    var minY: CGFloat {
        get {
            return self.rectangle!.minY
        }
    }
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["Start", "Stop", "MatrixTransform", "ObjectType", "Uuid", "Painter"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "Start":
            return self.start
        case "Stop":
            return self.stop
        case "MatrixTransform":
            return "1.0,0.0,0.0,1.0,0.0,0.0"
        case "Uuid":
            return self.Uuid
        case "Painter":
            return self.painter
        case "ObjectType":
            return "Ellipse"
        default:
            return nil
        }
    }
    
    required init(fromJson json: AnyObject) {
        if let _ = json as? NSDictionary {
            //Not yet implemented on the wall
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        self.start = aDecoder.decodeObjectForKey("start") as! CGAPoint?
        self.stop = aDecoder.decodeObjectForKey("stop") as! CGAPoint?
        self.Uuid = aDecoder.decodeObjectForKey("Uuid") as! String
        self.painter = aDecoder.decodeObjectForKey("painter") as! CGAPainter
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.start, forKey: "start")
        aCoder.encodeObject(self.stop, forKey: "stop")
        aCoder.encodeObject(self.Uuid, forKey: "Uuid")
        aCoder.encodeObject(self.painter, forKey: "painter")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(withPainter painter: CGAPainter) {
        super.init()
        
        self.generateUuid()
        
        self.painter = painter
    }
    
    func detected(touch touch: UITouch, withEvent event: UIEvent?) {
        if self.start != nil {
            self.stop = CGAPoint(fromTouch: touch)
        } else {
            self.start = CGAPoint(fromTouch: touch)
        }
    }
    
    func ended(touch touch: UITouch) {
        self.detected(touch: touch, withEvent: nil)
    }
    
    func draw(inContext context: CGContext) {
        if let rectangle = self.rectangle {
            CGContextSetStrokeColorWithColor(context, self.painter.color.CGColor)
            
            CGContextSetLineWidth(context, CGFloat(self.painter.thickness))
            
            CGContextAddEllipseInRect(context, rectangle)
            
            CGContextStrokePath(context)
        }
    }
    
    func isToErase(forTouch touch: UITouch) -> Bool {
        if let rectangle = self.rectangle {
            let point = touch.preciseLocationInView(touch.view)
            return point.x >= rectangle.minX && point.x <= rectangle.maxX && point.y >= rectangle.minY && point.y <= rectangle.maxY
        }
        
        return false
    }
    
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        self.painter.thickness *= Float(gesture.scale)
        
        self.start?.pinchToZoom(fromGesture: gesture)
        self.stop?.pinchToZoom(fromGesture: gesture)
    }
    
    func move(x x: CGFloat, y: CGFloat) {
        self.start?.move(x: x, y: y)
        self.stop?.move(x: x, y: y)
    }
}