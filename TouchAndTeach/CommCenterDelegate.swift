import Foundation

/**
 The CommCenterDelegate protocol defines the methods called by the CommCenter in response to the changement of the wall connection state and message reception
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
protocol CommCenterDelegate {
    func commCenter(commCenter commCenter: CommCenter, connectionStateChange connected: Bool)
    func commCenter(commCenter commCenter: CommCenter, receiveDocument document: Document)
}