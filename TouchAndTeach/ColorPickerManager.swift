import UIKit

/**
 Description
 ---
 Show a color and slider picker
 
 Changements from original code
 ---
 * Convert function names into Swift 3 API Design Guidelines
 * Add documentation
 * Add a delegate to send event (for example: the color changement)
 * Add a slider to add the thickness chooser
 * Modify the color change for change the color of the start of the view too
 * Bug fix (Array out of bound sometime append, the get color method was replaced by a calculation which can't finish out of the array)
 
 - authors: Michaël Minelli, Ethan Strider
 - seealso: [Original code](https://github.com/EthanStrider/ColorPickerExample)
 - version: 2.0.0
 */
class ColorPickerManager: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private static let WIDTH  = CGFloat(284)
    private static let HEIGHT = CGFloat(484)
	
	var color: UIColor = UIColor.blackColor()
    var callerWidth: CGFloat = 0.0
	var delegate: ColorPickerManagerDelegate?
    
    @IBOutlet weak var sliderThickness: UISlider!
    
    /**
     Set the start of the view background color
     */
    private func setArrowColor() {
        if let popoverController = self.popoverPresentationController {
            popoverController.sourceRect = CGRect(x: 0, y: -1, width: self.callerWidth, height: 0)
            popoverController.permittedArrowDirections = .Any
            popoverController.backgroundColor = self.color
            popoverController.delegate = nil
        }
    }
    
    /**
     Show the color picker
     
     - parameter sourceView: View which call this function (generally a button)
     - parameter width:      Width of the source view
     - parameter controller: The current view controller
     - parameter color:      Color actually selected
     - parameter thickness:  Thickness actually selected
     */
    func show(fromView sourceView: UIView, ofWidth width: CGFloat, withViewController controller: UIViewController, withColor color: UIColor, withThickness thickness: Float) {
        //Present the view
        self.callerWidth = width
        
        self.modalPresentationStyle = .Popover
        self.preferredContentSize = CGSizeMake(ColorPickerManager.WIDTH, ColorPickerManager.HEIGHT)
        
        if let popoverController = self.popoverPresentationController {
            popoverController.sourceView = sourceView
            self.setArrowColor()
        }
        
        controller.presentViewController(self, animated: true, completion: nil)
        
        self.view.backgroundColor = color

        //Init values
        self.color = color
        self.sliderThickness.value = thickness
    }
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 10
	}
    
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 16
	}
    
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) 
		cell.backgroundColor = UIColor.clearColor()
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		var colorPalette: Array<String>
		
		// Get colorPalette array from plist file
		let path = NSBundle.mainBundle().pathForResource("ColorPickerPalette", ofType: "plist")
		let pListArray = NSArray(contentsOfFile: path!)
		
		if let colorPalettePlistFile = pListArray {
			colorPalette = colorPalettePlistFile as! [String]

            let hexString = colorPalette[indexPath.section * 10 + indexPath.row] //Here is the bug fixe by the calculation of the color position in the plist file
            self.color = UIColor(hexString: hexString)!
            self.view.backgroundColor = self.color
            self.setArrowColor()
            
            //Inform the delegate
            self.delegate?.colorPicker(manager: self, selectNewColor: self.color)
        }
	}
    
    @IBAction func sliderThicknessChange(sender: UISlider) {
        //Inform the delegate
        self.delegate?.colorPicker(manager: self, selectNewThickness: sender.value)
    }
}