import Foundation
import UIKit

/**
 The CGAPainter class contains the property of a painter (color and size)
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGAPainter: NSObject, Serializable, Deserializable, NSCoding {
    var color:UIColor = UIColor.blackColor()
    var thickness:Float = 3.0
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["Color", "StrokeWidth"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "Color":
            return UInt(self.color.argb!)
        case "StrokeWidth":
            return Int(self.thickness)
        default:
            return ""
        }
    }
    
    required init(fromJson json: AnyObject) {
        if let json = json as? NSDictionary {
            if let colorInt = json["Color"] as? Int {
                let colorString = String(format: "%2X", colorInt)
                if let color = UIColor(hexString: colorString.substringFromIndex(colorString.startIndex.advancedBy(2))) {
                    self.color = color
                }
            }
            
            if let thickness = json["StrokeWidth"] as? Float {
                self.thickness = thickness
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        self.color = aDecoder.decodeObjectForKey("color") as! UIColor
        self.thickness = aDecoder.decodeFloatForKey("thickness")
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.color, forKey: "color")
        aCoder.encodeFloat(self.thickness, forKey: "thickness")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override init() {}
    
    init(color: UIColor, thickness: Float) {
        self.color = color
        self.thickness = thickness
    }
}