import Foundation

/**
 The UdpDiscoveryManagerDelegate protocol defines the methods called by the UdpDiscoveryManager in response to important events in the UDP discovery of the wall
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
protocol UdpDiscoveryManagerDelegate {
    func udpDiscovery(manager manager: UdpDiscoveryManager, receiveConnectionInformations connectionInformations: ConnectionInformations)
    func udpDiscovery(manager manager: UdpDiscoveryManager, connectionAttemptTimeout timeout: Double)
    func udpDiscovery(manager manager: UdpDiscoveryManager, errorOccurred error: UdpDiscoveryManager.Error)
}