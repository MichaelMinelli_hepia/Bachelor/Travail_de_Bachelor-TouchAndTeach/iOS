import Foundation
import UIKit
import CoreGraphics

import SCLAlertView
import SSZipArchive

/**
 The Document class describe a set of slides
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class Document: NSObject, NSCoding {
    /**
     The Document.Error enum contains all reasons that an error can append in this class
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    enum Error: ErrorType {
        case DirectoryCreation, UnZipFailed, ZipFailed, DirectoryContent, WrongNumberOfFiles, Unserialize, NoName, WriteFile, Unknown
    }
    
    var delegate: DocumentDelegate?
    var paintViewDelegate: PaintViewDelegate? {
        willSet {
            self.currentSlide.delegate = newValue
        }
    }
    
    private var slides: [PaintView] = []
    private var currentSlideIndexNotify = true //Specify if an action is needed when the slide index change
    private(set) var currentSlideIndex: Int = 0 {
        willSet{
            if self.currentSlideIndexNotify {
                self.currentSlide.delegate = nil
            }
        }
        didSet {
            if self.currentSlideIndexNotify {
                self.currentSlide.delegate = paintViewDelegate
            
                //Notify the delegate that the current slide have change
                self.delegate?.document(document: self, currentSlideDidChange: self.slides[self.currentSlideIndex])
            
                self.slides[self.currentSlideIndex].setNeedsDisplay()
            } else {
                self.currentSlideIndexNotify = true
            }
        }
    }
    
    var name: String = "" {
        didSet {
            //Notify the delegate that the name have change
            self.delegate?.document(document: self, changeName: self.name)
        }
    }
    var isStored: Bool = false
    
    var currentSlide: PaintView {
        return self.slides[self.currentSlideIndex]
    }
    
    var count: Int {
        return self.slides.count
    }
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    required init(coder aDecoder: NSCoder) {
        self.slides = aDecoder.decodeObjectForKey("slides") as! [PaintView]
        self.currentSlideIndex = aDecoder.decodeIntegerForKey("currentSlideIndex")
        self.name = aDecoder.decodeObjectForKey("name") as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.slides, forKey: "slides")
        aCoder.encodeInteger(self.currentSlideIndex, forKey: "currentSlideIndex")
        aCoder.encodeObject(self.name, forKey: "name")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override init() {
        super.init()
        
        self.createSlide(addItToSlidesList: true, setItLikeCurrentSlide: true)
    }
    
    /**
     Init a document with a TouchAndTeach file
     
     - parameter url: URL of the file
     */
    init(fromFile url: NSURL) throws {
        super.init()
        
        //Create the path where to unzip the TAT file
        let destinationPathTemp = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] + "/TAT_FILE_IMPORT_" + NSUUID().UUIDString
        let destinationURL = NSURL(fileURLWithPath: destinationPathTemp)
        
        //Create the directory
        do {
            try NSFileManager.defaultManager().createDirectoryAtURL(destinationURL, withIntermediateDirectories: true, attributes: nil)
        } catch {
            throw Error.DirectoryCreation
        }
        
        //Get paths
        guard let tatPath = url.path, let destinationPath = destinationURL.path else {
            throw Error.Unknown
        }
        
        //Unzip the file
        guard SSZipArchive.unzipFileAtPath(tatPath, toDestination: destinationPath) else {
            throw Error.UnZipFailed
        }
        
        //Get files include in TAT file
        var items: [String]
        do {
            items = try NSFileManager.defaultManager().contentsOfDirectoryAtPath(destinationPath)
        } catch {
            throw Error.DirectoryContent
        }
        
        guard items.count > 0 && items.count <= 2 else {
            throw Error.WrongNumberOfFiles
        }
        
        //Explore each file (actually pdf files are not supported)
        for filename in items {
            let file = NSURL(fileURLWithPath: destinationPath + "/" + filename)
            
            switch file.pathExtension! {
            case "json":
                //Deserialize the document (all slides)
                do {
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(NSData(contentsOfURL: file)!, options: NSJSONReadingOptions.MutableContainers)
                        
                    if let slides = jsonObject as? NSArray {
                        for slideJson in slides {
                            self.createSlide(fromJson: slideJson, addItToSlidesList: true, setItLikeCurrentSlide: false)
                        }
                    }
                    
                    
                } catch {
                    throw Error.Unserialize
                }
            default:
                break
            }
        }
        
        self.name = url.URLByDeletingPathExtension?.lastPathComponent ?? ""
    }
    
    /**
     Create a TouchAndTeach file from this document
     
     - returns: Path of the document (in cache)
     */
    func createTATFile() throws -> String {
        //Verify that the document have a name
        guard name != "" else {
            throw Error.NoName
        }
        
        //Define export paths
        let tatPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] + "/TAT_FILE_EXPORT_" + NSUUID().UUIDString
        let filesPath = tatPath + "/" + name
        
        let directoryCreationURL = NSURL(fileURLWithPath: filesPath)
        
        //Create directory
        do {
            try NSFileManager.defaultManager().createDirectoryAtURL(directoryCreationURL, withIntermediateDirectories: true, attributes: nil)
        } catch {
            throw Error.DirectoryCreation
        }
        
        //Create the JSON path
        let jsonFile = filesPath + "/drawobjects.json"
        
        //Serialize and store the document in the json file
        do {
            try Serialize.toJSON(fromArray: self.slides).writeToFile(jsonFile, atomically: true, encoding: NSUTF8StringEncoding)
        } catch {
            throw Error.WriteFile
        }
        
        //Create the zip file (tat file)
        let tatFile = tatPath + "/" + name + ".tat"
        guard SSZipArchive.createZipFileAtPath(tatFile, withContentsOfDirectory: filesPath) else {
            throw Error.Unserialize
        }
        
        return tatFile
    }
    
    /**
     Get a slide by his index
     
     - parameter index: Index of the wanted slide
     
     - returns: Search slide
     */
    func getSlide(at index: Int) -> PaintView? {
        if index < self.slides.count && index >= 0 {
            return self.slides[index]
        } else {
            return nil
        }
    }
    
    /**
     Create a slide
     
     - parameter fromJson: Possible JSON object representating the slide
     - parameter addItToSlidesList: Boolean that indicate if the slide have to be added to the slides list
     - parameter setLikeCurrentSlide: Boolean that indicate if the slide have to replace the current slide
     
     - returns: The created slide
     */
    func createSlide(fromJson json: AnyObject? = nil, addItToSlidesList addToSlidesList: Bool = false, setItLikeCurrentSlide currentSlide: Bool = false) -> PaintView {
        var paintView: PaintView
        if let json = json {
            paintView = PaintView(fromJson: json)
        } else {
            paintView = PaintView()
        }
        
        if addToSlidesList {
            let index = self.add(slide: paintView)
            
            if paintView.name == nil {
                paintView.name = "\(NSLocalizedString("SLIDE", comment: "SLIDE")) \(index+1)"
            }
        
            if currentSlide {
                self.currentSlideIndex = index
            }
        }
        
        return paintView
    }
    
    /**
     Add a slide to the list
     
     - parameter slide: Slide to add
     - parameter index: Optional position where to insert the slide
     
     - returns: The number of the added slide
     */
    func add(slide slide: PaintView, at index: Int? = nil) -> Int {
        if let index = index {
            self.slides.insert(slide, atIndex: index)
            return index
        } else {
            self.slides.append(slide)
            return self.slides.count - 1
        }
    }
    
    /**
     Copy a specifiq slide
     
     - parameter slideNumber: The slide number to copy
     */
    func copySlide(at slideNumber: Int) {
        if let slideToCopy = self.getSlide(at: slideNumber) {
            //Get datas from the slide to copy
            NSKeyedArchiver.setClassName("Slide", forClass: PaintView.self)
            let dataToCopy: NSData = NSKeyedArchiver.archivedDataWithRootObject(slideToCopy)
            
            //Create a copy from datas of the slide to copy
            NSKeyedUnarchiver.setClass(PaintView.self, forClassName: "Slide")
            if let slide = NSKeyedUnarchiver.unarchiveObjectWithData(dataToCopy) as? PaintView {
                //Change the name of the copy
                slide.name = NSLocalizedString("SLIDE_COPY_OF", comment: "SLIDE_COPY_OF") + (slide.name ?? "")
                
                //Add the slide
                self.add(slide: slide, at: slideNumber + 1)
                
                //Change the slide index for correspond to the right index
                if self.currentSlideIndex > slideNumber {
                    self.currentSlideIndexNotify = false
                    self.currentSlideIndex += 1
                }
            }
        }
    }
    
    /**
     Reorder the slides
     
     - parameter from: The slide position to move
     - parameter to:   The final position of the slide
     */
    func moveSlide(from start: Int, to end: Int) {
        if start >= 0 && end >= 0 && start < slides.count && end < slides.count && start != end {
            self.slides.insert(self.slides.removeAtIndex(start), atIndex: end)
            
            if self.currentSlideIndex == start {
                self.currentSlideIndex = end
            } else if (currentSlideIndex <= start && self.currentSlideIndex >= end) || (self.currentSlideIndex >= start && self.currentSlideIndex <= end) {
                if start < end {
                    self.currentSlideIndex -= 1
                } else {
                    self.currentSlideIndex += 1
                }
            }
        }
    }
    
    /**
     Change the current slide to the slide which have the number in parameter
     
     - note: If the slide taken slide number doesn't exists, the slide number 0 is choose
     
     - parameter newSlide: The slide number
     */
    func changeCurrentSlide(toNewSlideIndex newSlide: Int) {
        var destination = newSlide
        if newSlide >= self.count {
            destination = 0
        } else if newSlide < 0 {
            destination = self.count - 1
        }
        
        self.currentSlideIndex = destination
    }
    
    /**
     Remove a slide identified by his number
     
     - precondition: The slide number must exist otherwise nothing will be remove
     
     - note: 
     * If the removed slide is the current slide, the current slide become the slide number 0
     
     * We cannot remove the last slide because it must have always a minimum of une slide
     
     - parameter index: Number of the slide to delete
     */
    func removeSlide(at index: Int) {
        if self.count > 1 && self.count > index && index >= 0 {
            if index == self.currentSlideIndex {
                self.currentSlideIndex = 0
            } else if index < self.currentSlideIndex {
                self.currentSlideIndexNotify = false
                self.currentSlideIndex -= 1
            }
            
            //Remove after change the current slide number because it otherwise it can create a bug if the selected and to remove slide is the last slide
            self.slides.removeAtIndex(index)
        }
    }
    
    /**
     Set the frame value on all slides (to call when a frame changement is append)
     
     - parameter newFrame: The new frame dimensions
     - parameter window: The window into what the paintView have to be displayed
     */
    func set(frame newFrame: CGRect, andWindow window: UIWindow?) {
        currentSlide.frame = newFrame
        currentSlide.setNeedsDisplayWithFrozenImageUpdated()
        
        //Refresh all other slides but in background so the user can use the app in a minimum of time
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            for slide in self.slides {
                if self.currentSlide != slide {
                    slide.frame = newFrame
                    slide.updateFrozenImage(withWindow: window)
                
                    slide.setNeedsDisplayWithFrozenImageUpdated()
                }
            }
        })
    }
    
    /**
     Change the name of the document and possibily (optional) notify when changement have been done
     
     - parameter action: Handler for renaming
     */
    func rename(withCompletionHandler action: Optional<(_:String?) -> ()> = nil) {
        //Alert view for ask the user the new name
        let alert = SCLAlertView()
        let txt = alert.addTextField(NSLocalizedString("DOCUMENT_ENTER_NAME", comment: "DOCUMENT_ENTER_NAME"))
        txt.text = self.name
        alert.addButton(NSLocalizedString("DOCUMENT_ENTER_NAME_BUTTON", comment: "DOCUMENT_ENTER_NAME_BUTTON")) {
            if let text = txt.text {
                self.name = text
                
                if let action = action {
                    action(text)
                }
            } else {
                if let action = action {
                    action(nil)
                }
            }
        }
        alert.showCustom(NSLocalizedString("DOCUMENT_ENTER_NAME_TITLE", comment: "DOCUMENT_ENTER_NAME_TITLE"), subTitle: NSLocalizedString("DOCUMENT_ENTER_NAME_MSG", comment: "DOCUMENT_ENTER_NAME_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconName")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
    }
    
    /**
     Refresh the document after a changement
     
     - parameter delegate: Delegate to call for indicate the changements and get the frame value
     */
    func refresh(withDelegate delegate: DocumentDelegate?, withPaintViewDelegate paintViewDelegate: PaintViewDelegate?) {
        self.delegate = delegate
        self.paintViewDelegate = paintViewDelegate
        
        if let delegate = delegate {
            self.set(frame: delegate.documentNeedUpdatedFrame(document: self), andWindow: delegate.documentNeedWindow(document: self))
            delegate.document(document: self, currentSlideDidChange: self.slides[self.currentSlideIndex])
            delegate.document(document: self, changeName: self.name)
        }
    }
}