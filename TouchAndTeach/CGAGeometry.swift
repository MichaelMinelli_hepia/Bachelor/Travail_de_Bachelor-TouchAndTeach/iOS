import Foundation
import UIKit

/**
 The CGAGeometry protocol describe function that a Geometric Object (or Draw Object) must have
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
@objc protocol CGAGeometry : class {
    var Uuid: String { get set }
    var painter:CGAPainter { get }
    
    var minX: CGFloat { get }
    var minY: CGFloat { get }
    
    /**
     Function called when a touch is detected and have to add this to the object
     
     - parameter touch: Touch object
     - parameter event: Optional event
     */
    func detected(touch touch: UITouch, withEvent event: UIEvent?)
    
    /**
     Function called when a touch is the last touch which is detected for this object
     
     - parameter touch: Touch object
     */
    func ended(touch touch: UITouch)
    
    
    /**
     Print the object in a context
     
     - parameter context: The destination context
     */
    func draw(inContext context: CGContext)
    
    /**
     Verify if the object have to be erased in function of the position of the touch
     
     - parameter touch: Touch object
     
     - returns: Boolean which indicate if the erase is needed
     */
    func isToErase(forTouch touch: UITouch) -> Bool
    
    
    /**
     Call to a zoom
     
     - parameter gesture: Pinch gesture object wich determinate the zoom quantity
     */
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer)
    
    /**
     Move the object coordinates
     
     - parameter x: X deviation
     - parameter y: Y deviation
     */
    func move(x x: CGFloat, y: CGFloat)
    
    /**
     Function called just before this object will be erased
     
     - note: This function is optional
     */
    optional func erase()
}

//Method for do a similar to Abstract class in Swift 2.x
extension CGAGeometry {
    /**
     Generate the UUID of the object
     */
    func generateUuid() {
        let UUID_LENGTH = 10
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: UUID_LENGTH)
        
        //Create the string letter by letter
        for _ in 0 ..< UUID_LENGTH {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        self.Uuid = String(randomString)
    }
    
    /**
     Test if an intersection exists between a touch and two points
     
     - seealso: [Source method](http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect)
     
     - parameter touch:  Touch object
     - parameter point1: First point
     - parameter point2: Second point
     
     - returns: Boolean that is true if an intersection exists
     */
    func isIntersectionExist(betweenTouch touch: UITouch, andPoint point1: CGAPoint, andPoint point2: CGAPoint) -> Bool {
        //Verify that the touch have a previous location
        if touch.precisePreviousLocationInView(touch.view!) != touch.preciseLocationInView(touch.view!) {
            let x0 = point1.preciseLocation.x
            let y0 = point1.preciseLocation.y
            let x1 = point2.preciseLocation.x
            let y1 = point2.preciseLocation.y
            
            let x2 = touch.precisePreviousLocationInView(touch.view!).x
            let y2 = touch.precisePreviousLocationInView(touch.view!).y
            let x3 = touch.preciseLocationInView(touch.view!).x
            let y3 = touch.preciseLocationInView(touch.view!).y
            
            let s1_x = x1 - x0
            let s1_y = y1 - y0
            let s2_x = x3 - x2
            let s2_y = y3 - y2
            
            let s = (-s1_y * (x0 - x2) + s1_x * (y0 - y2)) / (-s2_x * s1_y + s1_x * s2_y)
            let t = ( s2_x * (y0 - y2) - s2_y * (x0 - x2)) / (-s2_x * s1_y + s1_x * s2_y)
            
            return s >= 0 && s <= 1 && t >= 0 && t <= 1
        }
        
        return false
    }
}