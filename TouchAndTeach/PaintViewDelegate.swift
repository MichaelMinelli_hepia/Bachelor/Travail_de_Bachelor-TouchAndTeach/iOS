import Foundation
import CoreGraphics

/**
 The PaintViewDelegate protocol defines the methods called by the PaintView (Slide) in response to important events

 - author: Michaël Minelli
 - version: 1.0.0
 */

protocol PaintViewDelegate {
    func paintView(manager: PaintView, changeName newName: String?)
}