import Foundation
import UIKit

/**
 The MenuController class is the view controller of right and left menu.
 
 - note: It's not the direct view controller showed but just a step for don't have to rewrite same code on the right and the left menu
 - author: Michaël Minelli
 - version: 1.0.0
 */
class MenuController: UIViewController {
    override func initGlobalAppearance() {
        //Add a tint color for the blur effect
        self.view.backgroundColor = Colors.menuPrimaryColor
        //self.view.backgroundColor = UIColor.clearColor()
        
        //Add blur effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        self.view.insertSubview(blurEffectView, atIndex: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initGlobalAppearance()
    }
}