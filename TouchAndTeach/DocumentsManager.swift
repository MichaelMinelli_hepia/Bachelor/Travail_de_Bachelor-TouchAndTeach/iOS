import Foundation
import UIKit

import SCLAlertView

/**
 The singleton DocumentsManager class is the manager of stored documents
 
 - note: This class is a singleton
 - author: Michaël Minelli
 - version: 1.0.0
 */
public class DocumentsManager {
    /**
     The DocumentsManager.Error enum contains all reasons that an error can append
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    enum Error: ErrorType {
        case NoName, DocumentAlreadyExist, DocumentNotExist
    }
    
    private static let DOCUMENTS_LIST_STOR_ID = "ListDocumentsStor"
    private static let DOCUMENTS_LIST_STOR_DEFAULT: [DocumentRepresentation] = []
    
    var documentToSave: Document?
    var toastView: UIView?
    
    private let standardDefault = NSUserDefaults.standardUserDefaults()
    
    private var documentsList: [DocumentRepresentation]?
    
    var getDocumentsList: [DocumentRepresentation] {
        get {
            if self.documentsList == nil {
                NSKeyedUnarchiver.setClass(DocumentRepresentation.self, forClassName: "DocumentRepresentation")
                if let documentsList = self.standardDefault.objectForKey(DocumentsManager.DOCUMENTS_LIST_STOR_ID) as? NSData {
                    self.documentsList = NSKeyedUnarchiver.unarchiveObjectWithData(documentsList) as? [DocumentRepresentation]
                }
            }
        
            return self.documentsList ?? []
        }
    }
    
    private init() {
        if self.standardDefault.objectForKey(DocumentsManager.DOCUMENTS_LIST_STOR_ID) == nil {
            self.documentsList = []
            self.storeDocumentsList()
        } else {
            //Provoke the load of the document list
            let _ = self.getDocumentsList
        }
    }
    static let sharedInstance = DocumentsManager()
    
    /**
     Store the document list
     */
    private func storeDocumentsList() {
        if let documentsList = self.documentsList {
            self.documentsList = documentsList.sort({ $0.lastSave.compare($1.lastSave) == NSComparisonResult.OrderedDescending})
            
            NSKeyedArchiver.setClassName("DocumentRepresentation", forClass: DocumentRepresentation.self)
            let dataToStore: NSData = NSKeyedArchiver.archivedDataWithRootObject(documentsList)
            self.standardDefault.setObject(dataToStore, forKey: DocumentsManager.DOCUMENTS_LIST_STOR_ID)
            self.standardDefault.synchronize()
        }
    }
    
    /**
     Indicate if a document exist in the list of stored documents
     
     - parameter name: Name of the search document
     
     - returns: Boolean that indicate if the document were found
     */
    private func isDocumentExist(withName name: String) -> Bool {
        return self.documentsList?.contains({ $0.name == name }) ?? false
    }
    
    /**
     Save a document
     
     - parameter document:      Document to save
     - parameter allowOverride: Boolea n that indicate if the user want to override a possible document with the same name
     
     - throws: 
        * Error that indicate the document taken have no name
        * Error that indicate an another document with the same name and the parameter override to false
     */
    func save(document document: Document, allowOverride: Bool = false) throws {
        if document.name == "" {
            throw Error.NoName
        }
        
        //Verify if the document already exist and if the user want to override it or not
        if self.isDocumentExist(withName: document.name) {
            if !allowOverride && !document.isStored {
                throw Error.DocumentAlreadyExist
            } else {
                do {
                    try self.removeDocument(withName: document.name)
                } catch {}
            }
        }
        
        //Update the stored document list
        self.documentsList?.append(DocumentRepresentation(withDocument: document))
        self.storeDocumentsList()
        
        //Store the document
        NSKeyedArchiver.setClassName("Document", forClass: Document.self)
        let dataToStore: NSData = NSKeyedArchiver.archivedDataWithRootObject(document)
        self.standardDefault.setObject(dataToStore, forKey: document.name)
    }
    
    /**
     Ask the user to save the document by first ask to validate the name
     
     - parameter document: Document to save
     - parameter view:     View for display toast messages
     */
    func askSave(document document: Document, onView view: UIView) {
        self.documentToSave = document
        self.toastView = view
        
        document.rename(withCompletionHandler: self.saveAfterRename)
    }
    
    /**
     Save the document if possible
     */
    private func saveAfterRename(withNewName newName: String?) {
        if let document = self.documentToSave {
            do {
                try DocumentsManager.sharedInstance.save(document: document)
                
                self.toastView?.makeToast(NSLocalizedString("DOCUMENT_SAVED", comment: "DOCUMENT_SAVED"))
            } catch DocumentsManager.Error.DocumentAlreadyExist {
                //If the document already exist, ask the user for override it
                let alert = SCLAlertView()
                alert.addButton(NSLocalizedString("YES", comment: "YES")) {
                    do {
                        try DocumentsManager.sharedInstance.save(document: document, allowOverride: true)
                        
                        self.toastView?.makeToast(NSLocalizedString("DOCUMENT_SAVED", comment: "DOCUMENT_SAVED"))
                    } catch {}
                }
                alert.showCustom(NSLocalizedString("DOCUMENT_ALREADY_EXISTS_TITLE", comment: "DOCUMENT_ALREADY_EXISTS_TITLE"), subTitle: String(format: NSLocalizedString("DOCUMENT_ALREADY_EXISTS_MSG", comment: "DOCUMENT_ALREADY_EXISTS_MSG"), Global_iOS.sharedInstance.documentController.currentDocument.name), color: Colors.primaryColor, icon: UIImage(named: "iconSave")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
            } catch DocumentsManager.Error.NoName {
                self.toastView?.makeToast(NSLocalizedString("DOCUMENT_NAME_NEEDED", comment: "DOCUMENT_NAME_NEEDED"))
            } catch {}
        }
        
        self.documentToSave = nil
        self.toastView = nil
    }
    
    /**
     Load a document identified by his name
     
     - parameter name: Name of the document
     
     - returns: Possibly the document searched (optional)
     */
    func loadDocument(withName name: String) -> Document? {
        NSKeyedUnarchiver.setClass(Document.self, forClassName: "Document")
        if let dataToLoad = self.standardDefault.objectForKey(name) as? NSData {
            if let document = NSKeyedUnarchiver.unarchiveObjectWithData(dataToLoad) as? Document {
                document.isStored = true
                return document
            }
        }
        
        return nil
    }
    
    /**
     Remove a document identified by his name
     
     - parameter name: Name of the document
     
     - throws: Error that indicate that the document does not exists
     */
    func removeDocument(withName name: String) throws {
        if let _ = self.documentsList {
            if let index = self.documentsList!.indexOf({ $0.name == name }) {
                //Remove the document from the list and from the storage
                self.documentsList!.removeAtIndex(index)
                self.standardDefault.removeObjectForKey(name)
                
                self.storeDocumentsList()
            } else {
                throw Error.DocumentNotExist
            }
        }
    }
}