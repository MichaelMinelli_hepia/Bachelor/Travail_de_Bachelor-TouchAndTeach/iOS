import Foundation
import CocoaAsyncSocket

/**
 The TcpCommunicationManager class is used for TCP socket communication
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class TcpCommunicationManager: GCDAsyncSocketDelegate {
    static let CONNECTION_TIMEOUT = 5.0
    
    /**
     The TcpCommunicationManager.Error enum contains all reasons that an error can append
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    enum Error {
        case AlreadyConnected, NotConnected, Unknown
    }
    
    var delegate: TcpCommunicationManagerDelegate?
    
    private var tcpSocket: GCDAsyncSocket?
    
    private var host: ConnectionInformations?
    
    /**
     Indicate if the socket is connected or not
    */
    var isConnected: Bool {
        if let tcpSocket = self.tcpSocket where tcpSocket.isConnected {
            return true
        }
        
        return false
    }
    
    /**
     Close the socket and delete the reference to it
     */
    private func destroySocket() {
        self.tcpSocket?.disconnect()
        self.tcpSocket = nil
    }
    
    /**
     Called when an error occured
     
     - parameter error: Error type to send to the delegate
     */
    private func sendError(withDescription error: Error) {
        self.delegate?.tcpCommunication(manager: self, errorOccurred: error)
        self.destroySocket()
    }
    
    /**
     Start read the socket entry until "\r\n" is received
     */
    private func startReadString() {
        self.tcpSocket?.readDataToData("\r\n".dataUsingEncoding(NSASCIIStringEncoding), withTimeout: -1, tag: -1)
    }
    
    /**
     Start read the socket until a specified length of bytes
     
     - parameter length: bytes to read
     */
    func startReadData(toLength length: UInt) {
        self.tcpSocket?.readDataToLength(length, withTimeout: -1, tag: -1)
    }
    
    /**
     Start a connection attempt
     
     - parameter hostInfo: Server network informations (ip, port)
     */
    func connect(toHost hostInfo: ConnectionInformations) {
        //Create the socket if not exist
        if self.tcpSocket == nil {
            self.tcpSocket = GCDAsyncSocket(delegate: self, delegateQueue: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0))
        }
        
        //If not already connected attempt to connect
        if !self.isConnected {
            do {
                try self.tcpSocket?.connectToHost(hostInfo.ip, onPort: hostInfo.port, withTimeout: TcpCommunicationManager.CONNECTION_TIMEOUT)
            } catch {
                self.sendError(withDescription: .Unknown)
                return
            }
        } else {
            self.sendError(withDescription: .AlreadyConnected)
        }
    }
    
    /**
     Disconnect from the server
     */
    func disconnect() {
        self.destroySocket()
    }
    
    @objc func socket(sock: GCDAsyncSocket!, didConnectToHost host: String!, port: UInt16) {
        self.startReadString()
        
        self.host = ConnectionInformations(ip: host, port: port)
        self.delegate?.tcpCommunication(manager: self, isConnectedToHost: self.host!)
    }
    
    @objc func socketDidDisconnect(sock: GCDAsyncSocket!, withError err: NSError!) {
        self.delegate?.tcpCommunication(manager: self, isDisconnectedFromHost: self.host!)
    }
    
    @objc func socket(sock: GCDAsyncSocket!, didReadData data: NSData!, withTag tag: Int) {
        self.delegate?.tcpCommunication(manager: self, receiveMessage: data)
        
        self.startReadString()
    }
    
    /**
     Send a message into the socket
     
     - parameter message: Datas to send
     */
    func send(message message: NSData) {
        if self.isConnected {
            self.tcpSocket!.writeData(message, withTimeout: -1, tag: -1)
        } else {
            self.sendError(withDescription: .NotConnected)
        }
    }
}