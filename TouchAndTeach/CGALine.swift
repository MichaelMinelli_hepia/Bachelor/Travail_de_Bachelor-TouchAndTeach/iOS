import UIKit
import Foundation

/**
 The CGALine class describe a drawed line
 
 - note: For functions commentaries look at the CGAGeometry class commentaries
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGALine: NSObject, CGAGeometry, Serializable, Deserializable, NSCoding {
    private static let MIN_SIZE_STYLUS = 10
    private static let MIN_SIZE_FINGER = 3
    private static let MIN_SIZE_MODE_REGION = 50
    
    var points = [CGAPoint]()
    
    var Uuid: String = ""
    
    private(set) var painter:CGAPainter = CGAPainter()
    
    //Coordinate of the line region
    private var region: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    private var leftPoint: CGAPoint? {
        didSet{
            self.updateRegion()
        }
    }
    private var rightPoint: CGAPoint? {
        didSet{
            self.updateRegion()
        }
    }
    private var topPoint: CGAPoint? {
        didSet{
            self.updateRegion()
        }
    }
    private var bottomPoint: CGAPoint? {
        didSet{
            self.updateRegion()
        }
    }
    
    var minX: CGFloat {
        get {
            return self.leftPoint?.preciseLocation.x ?? CGFloat.infinity
        }
    }
    var minY: CGFloat {
        get {
            return self.topPoint?.preciseLocation.y ?? CGFloat.infinity
        }
    }
    
    //Store the type of the touch (pencil ou finger)
    private var touchType: UITouchType?
    
    /**
     This var indicate if the current line is desplayable (test the min size)
     */
    private var isDiplayable: Bool {
        get {
            var minSize = CGALine.MIN_SIZE_FINGER
            if let touchType = self.touchType where touchType == .Stylus {
                minSize = CGALine.MIN_SIZE_STYLUS
            }
            
            return self.points.count >= minSize
        }
    }
    
    /**
     Update the CGRect object that represent the region with the stored coordinates
     */
    private func updateRegion() {
        if let leftPoint = self.leftPoint, let rightPoint = self.rightPoint, let topPoint = self.topPoint, let bottomPoint = self.bottomPoint {
            self.region = CGRect(x: leftPoint.preciseLocation.x, y: topPoint.preciseLocation.y, width: rightPoint.preciseLocation.x - leftPoint.preciseLocation.x, height: bottomPoint.preciseLocation.y - topPoint.preciseLocation.y)
        } else {
            self.region = CGRect(x: 0, y: 0, width: 0, height: 0)
        }
    }
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["PointList", "MatrixTransform", "ObjectType", "StrokeType", "Uuid", "Painter"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "PointList":
            return self.isDiplayable ? self.points : []
        case "MatrixTransform":
            return "1.0,0.0,0.0,1.0,0.0,0.0"
        case "Uuid":
            return self.Uuid
        case "Painter":
            return self.painter
        case "ObjectType":
            return "Stroke"
        case "StrokeType":
            return "HighDensity"
        default:
            return nil
        }
    }
    
    required init(fromJson json: AnyObject) {
        super.init()
        
        if let json = json as? NSDictionary {
            var matrixTransform = CGAffineTransformIdentity
            if let matrix = json["MatrixTransform"] as? String {
                let matrixTable = matrix.componentsSeparatedByString(",")
                
                if matrixTable.count == 6 {
                    matrixTransform = CGAffineTransform(a: CGFloat(Float(matrixTable[0])!), b: CGFloat(Float(matrixTable[1])!), c: CGFloat(Float(matrixTable[2])!), d: CGFloat(Float(matrixTable[3])!), tx: CGFloat(Float(matrixTable[4])!), ty: CGFloat(Float(matrixTable[5])!))
                }
            }
            
            if let pointList = json["PointList"] as? NSArray {
                for pointJson in pointList {
                    let point = CGAPoint(fromJson: pointJson)
                    point.apply(transformationMatrix: matrixTransform)
                    self.points.append(point)
                }
            }
            
            if let painter = json["Painter"] as? NSDictionary {
                self.painter = CGAPainter(fromJson: painter)
            }
            
            if let uuid = json["Uuid"] as? String {
                self.Uuid = uuid
            }
        }
        
        self.recreateRegion()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        self.points = aDecoder.decodeObjectForKey("points") as! [CGAPoint]
        self.Uuid = aDecoder.decodeObjectForKey("Uuid") as! String
        self.painter = aDecoder.decodeObjectForKey("painter") as! CGAPainter
        
        self.recreateRegion()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.points, forKey: "points")
        aCoder.encodeObject(self.Uuid, forKey: "Uuid")
        aCoder.encodeObject(self.painter, forKey: "painter")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(withPainter painter: CGAPainter) {
        super.init()
        
        self.generateUuid()
        
        self.painter = painter
    }
    
    func detected(touch touch: UITouch, withEvent event: UIEvent?) {
        /*
         Add coalesced touch it's add more point (interpreted points) so a smouth line when zooming 
         
         - seealso: https://developer.apple.com/videos/play/wwdc2015/233/
         */
        let coalescedTouches = event?.coalescedTouchesForTouch(touch) ?? [touch]
        self.addPoints(fromTouches: coalescedTouches)
    }
    
    func ended(touch touch: UITouch) {
        self.detected(touch: touch, withEvent: nil)
    }
    
    /**
     Recreate the region by test all point for find all limit points
     */
    private func recreateRegion() {
        for point in points {
            self.test(point: point)
        }
    }
    
    /**
     Test a point for determinate if it's a limit point (top left, bottom left, top right, botton right)
     
     - parameter point: The CGAPoint to test
     */
    private func test(point point: CGAPoint) {
        if let leftPoint = leftPoint, let rightPoint = rightPoint, let topPoint = topPoint, let bottomPoint = bottomPoint {
            if point.preciseLocation.x < leftPoint.preciseLocation.x {
                self.leftPoint = point
            }
            
            if point.preciseLocation.x > rightPoint.preciseLocation.x {
                self.rightPoint = point
            }
            
            if point.preciseLocation.y < topPoint.preciseLocation.y {
                self.topPoint = point
            }
            
            if point.preciseLocation.y > bottomPoint.preciseLocation.y {
                self.bottomPoint = point
            }
        } else {
            self.leftPoint = point
            self.rightPoint = point
            self.topPoint = point
            self.bottomPoint = point
        }
    }
    
    /**
     Add points to the line
     
     - parameter touches: Points to add (UITouch object)
     
     - returns: The table of CGAPoint created and added to the object
     */
    private func addPoints(fromTouches touches: [UITouch]) -> [CGAPoint] {
        var pointsAdded: [CGAPoint] = []
        
        for touch in touches {
            let point = CGAPoint(fromTouch: touch)
            
            if self.touchType == nil {
                self.touchType = point.type
            }
            
            self.points.append(point)
            pointsAdded.append(point)
            
            self.test(point: point)
        }
        
        return pointsAdded
    }
    
    func draw(inContext context: CGContext) {
        if isDiplayable {
            var maybePriorPoint: CGAPoint?
            
            for point in self.points {
                //verify if there is a previous point
                guard let priorPoint = maybePriorPoint else {
                    maybePriorPoint = point
                    continue
                }
                
                CGContextSetStrokeColorWithColor(context, painter.color.CGColor)
                
                CGContextBeginPath(context)
                
                //Add a line from previous point to the new
                CGContextSetLineCap(context, CGLineCap.Round)
                CGContextMoveToPoint(context, priorPoint.preciseLocation.x, priorPoint.preciseLocation.y)
                CGContextAddLineToPoint(context, point.preciseLocation.x, point.preciseLocation.y)
                
                //Example to use the magnitude (Apple pencil only) in the display of the line
                //CGContextSetLineWidth(context, self.thickness+self.thickness/2*point.magnitude)
                CGContextSetLineWidth(context, CGFloat(painter.thickness))
                CGContextStrokePath(context)
                
                maybePriorPoint = point
            }
        }

        /*
        //Show the region (for debug)
        CGContextSetStrokeColorWithColor(context, painter.color.CGColor)
        
        CGContextSetLineWidth(context, CGFloat(painter.thickness))
        
        CGContextStrokeRect(context, region)
        */
    }
    
    func isToErase(forTouch touch: UITouch) -> Bool {
        if points.count < CGALine.MIN_SIZE_MODE_REGION || self.region.contains(touch.preciseLocationInView(touch.view!)) {
            var priorPoint: CGAPoint?
            
            //test all part of the line if there is an intersection
            for point in self.points {
                if let priorPoint = priorPoint {
                    if self.isIntersectionExist(betweenTouch: touch, andPoint: priorPoint, andPoint: point) {
                        return true
                    }
                }
                
                priorPoint = point
            }
        }
        
        return !self.isDiplayable
    }
    
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        //Adapt the thickness too
        self.painter.thickness *= Float(gesture.scale)
        
        //Adapt the position of each point
        for point in self.points {
            point.pinchToZoom(fromGesture: gesture)
        }
        
        self.updateRegion()
    }
    
    func move(x x: CGFloat, y: CGFloat) {
        for point in self.points {
            point.move(x: x, y: y)
        }
        
        self.updateRegion()
    }
}

/**
 The CGASmoothLine class describe a drawed line with smooth angle for finger touches
 
 - note: For functions commentaries look at the CGAGeometry class commentaries and CGALine
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGASmoothLine: CGALine {
    private var path: CGMutablePathRef = CGPathCreateMutable()
    private var previousPoint: CGAPoint?
    private var previousPreviousPoint: CGAPoint?
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    override func jsonValue(forKey key: String) -> AnyObject? {
        if key == "StrokeType" {
            return "LowDensity"
        } else {
            return super.jsonValue(forKey: key)
        }
    }
    
    required init(fromJson json: AnyObject) {
        super.init(fromJson: json)
        
        self.recreatePath()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.recreatePath()
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override init(withPainter painter: CGAPainter) {
        super.init(withPainter: painter)
    }
    
    override private func addPoints(fromTouches touches: [UITouch]) -> [CGAPoint] {
        let pointsAdded = super.addPoints(fromTouches: touches)
        
        for point in pointsAdded {
            self.addToPath(point)
        }
        
        return pointsAdded
    }
    
    /**
     Calculate the middle of two points
     
     - parameter p1: Point 1
     - parameter p2: Point 2
     
     - returns: The point in the middle
     */
    private func getMidPoint(betweemPoint p1: CGAPoint, andPoint p2: CGAPoint) -> CGPoint {
        return CGPointMake((p1.preciseLocation.x + p2.preciseLocation.x) * 0.5, (p1.preciseLocation.y + p2.preciseLocation.y) * 0.5)
    }
    
    /**
     Add a point to the path to display
     
     - parameter point: Point to add
     */
    private func addToPath(point: CGAPoint) {
        if self.previousPoint == nil {
            self.previousPoint = point
            self.previousPreviousPoint = point
        }
        
        //Update points: previousPrevious -> mid1 -> previous -> mid2 -> current
        let mid1: CGPoint = getMidPoint(betweemPoint: self.previousPoint!, andPoint: self.previousPreviousPoint!);
        let mid2: CGPoint = getMidPoint(betweemPoint: point, andPoint: self.previousPoint!);
        
        //To represent the finger movement, create a new path segment, a quadratic bezier path from mid1 to mid2, using previous as a control point
        let subpath: CGMutablePathRef = CGPathCreateMutable();
        CGPathMoveToPoint(subpath, nil, mid1.x, mid1.y);
        CGPathAddQuadCurveToPoint(subpath, nil, self.previousPoint!.preciseLocation.x, self.previousPoint!.preciseLocation.y, mid2.x, mid2.y);
        
        //Append the quad curve to the accumulated path so far.
        CGPathAddPath(self.path, nil, subpath)
        
        self.previousPreviousPoint = self.previousPoint
        self.previousPoint = point
    }
    
    /**
     Recreate the path by read all stored points
     */
    private func recreatePath() {
        self.path = CGPathCreateMutable()
        self.previousPoint = nil
        self.previousPreviousPoint = nil
        
        for point in self.points {
            self.addToPath(point)
        }
    }
    
    override func draw(inContext context: CGContext) {
        if self.isDiplayable {
            CGContextAddPath(context, self.path)
            CGContextSetLineCap(context, CGLineCap.Round)
            CGContextSetLineWidth(context, CGFloat(self.painter.thickness))
            CGContextSetStrokeColorWithColor(context, self.painter.color.CGColor)
            
            CGContextStrokePath(context)
            
            /*
             //Show the region (for debug)
             CGContextSetStrokeColorWithColor(context, self.painter.color.CGColor)
             
             CGContextSetLineWidth(context, CGFloat(self.painter.thickness))
             
             CGContextStrokeRect(context, self.region)
             */
        }
    }
    
    override func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        super.pinchToZoom(fromGesture: gesture)
        
        self.recreatePath()
    }
    
    override func move(x x: CGFloat, y: CGFloat) {
        super.move(x: x, y: y)
        
        self.recreatePath()
    }
}