import Foundation
import UIKit

import SCLAlertView
import SlideMenuControllerSwift

/**
 The UIViewController extension add some function which have to be accessible from all view controllers
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
extension UIViewController {
    /**
     Initialization of the appearance of views controllers
     */
    func initGlobalAppearance() {
        Global_iOS.sharedInstance.commCenter!.toastView = self.view
        
        if let navigationController = self.navigationController {
            navigationController.navigationBar.backgroundColor = Colors.primaryColor
            navigationController.navigationBar.barTintColor = Colors.primaryColor
            navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Colors.secondaryColor]
            navigationController.navigationBar.tintColor = Colors.secondaryColor
            navigationController.navigationBar.translucent = false
        }
    }
    
    /**
     Creation of all neavigation bar item (buttons)
     
     - parameter viewName: Name of the current view controller
     */
    func setNavigationBarItem(withName viewName: String, isConnected connected: Bool) {
        dispatch_async(dispatch_get_main_queue(), {
            //Add the left button for open the left menu
            let btLeftMenu = UIButton()
            btLeftMenu.setImage(UIImage(named: "iconMenu")!.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
            btLeftMenu.setTitle(" \(viewName)", forState: .Normal)
            btLeftMenu.titleLabel?.font = UIFont.systemFontOfSize(15.0)
            btLeftMenu.sizeToFit()
            btLeftMenu.addTarget(self, action: #selector(self.slideMenuController()!.toggleLeft), forControlEvents: .TouchUpInside)
            
            let leftBarButton = UIBarButtonItem(customView: btLeftMenu)
            self.navigationItem.leftBarButtonItem = leftBarButton;
            
            //Add the right button for the right menu
            let btSlide = UIButton()
            btSlide.setImage(UIImage(named: "iconSlide")!.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
            btSlide.setTitle(" \(NSLocalizedString("MENU_SLIDE", comment: "MENU_SLIDE"))", forState: .Normal)
            btSlide.titleLabel?.font = UIFont.systemFontOfSize(10.0)
            btSlide.sizeToFit()
            btSlide.addTarget(self, action: #selector(self.slideMenuController()!.toggleRight), forControlEvents: .TouchUpInside)
            
            let viewSeparator = UIView(frame: CGRectMake(0, 0, 15, 50))
            
            //Add the right button for connection
            let btConnect = UIButton()
            var connectionImage = "iconConnectionState_Disconnected"
            if connected {
                connectionImage = "iconSendSlide"
            }
            btConnect.setImage(UIImage(named: connectionImage)!.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
            btConnect.sizeToFit()
            btConnect.addTarget(self, action: #selector(self.btConnectSendClick), forControlEvents: .TouchUpInside)
            
            //Add the right button for connection
            let btShare = UIButton()
            btShare.setImage(UIImage(named: "iconShare")!.image(withColor: Colors.secondaryColor).image(scaledToSize: CGSize(width: 18, height: 24)), forState: UIControlState.Normal)
            btShare.sizeToFit()
            btShare.addTarget(self, action: #selector(self.btShareClick), forControlEvents: .TouchUpInside)
            
            self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: btSlide), UIBarButtonItem(customView: viewSeparator), UIBarButtonItem(customView: btConnect), UIBarButtonItem(customView: viewSeparator), UIBarButtonItem(customView: btShare)];
            
            //Add move gesture for open the menu
            self.slideMenuController()?.removeLeftGestures()
            self.slideMenuController()?.removeRightGestures()
            self.slideMenuController()?.addLeftGestures()
            self.slideMenuController()?.addRightGestures()
        })
    }
    
    /**
     Remove all items from the navigation bar
     */
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    /**
     Action called when the connect button is pressed
     */
    func btConnectSendClick() {
        if Global_iOS.sharedInstance.commCenter!.state == .Disconnected {
            Global_iOS.sharedInstance.commCenter!.connect()
        } else {
            Global_iOS.sharedInstance.commCenter?.send(oneSlide: Global_iOS.sharedInstance.documentController.currentDocument.currentSlide)
        }
    }
    
    /**
     Action called when the share button is pressed
     */
    func btShareClick() {
        //Verify that the document have a name and ask for it
        if Global_iOS.sharedInstance.documentController.currentDocument.name == "" {
            Global_iOS.sharedInstance.documentController.currentDocument.rename(withCompletionHandler: self.shareDocument)
        } else {
            self.shareDocument(withName: Global_iOS.sharedInstance.documentController.currentDocument.name)
        }
    }
    
    /**
     Create the document and display the share menu
     
     - parameter name: Name of the document
     */
    func shareDocument(withName name: String?) {
        if name != "" {
            //Display waiting image
            self.view.makeToastActivity(.Center)
            
            //Execute the file save in an another thread
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                do {
                    let file = try Global_iOS.sharedInstance.documentController.currentDocument.createTATFile()
                    let fileURL = NSURL(fileURLWithPath: file)
                    
                    let activityViewController = UIActivityViewController(activityItems: [fileURL], applicationActivities: nil)
                    activityViewController.popoverPresentationController!.barButtonItem = self.navigationItem.rightBarButtonItems![self.navigationItem.rightBarButtonItems!.count-1]
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        //Dsiplay the share menu
                        self.view.hideToastActivity()
                        self.presentViewController(activityViewController, animated: true, completion: {})
                    })
                } catch {
                    self.view.makeToast(NSLocalizedString("ERROR", comment: "ERROR"))
                    return
                }
            })
        } else {
            //Display no name error
            SCLAlertView().showCustom(NSLocalizedString("SHARE_NO_NAME_TITLE", comment: "SHARE_NO_NAME_TITLE"), subTitle: NSLocalizedString("SHARE_NO_NAME_MSG", comment: "SHARE_NO_NAME_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconDelete")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("OK", comment: "OK"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
        }
    }
}