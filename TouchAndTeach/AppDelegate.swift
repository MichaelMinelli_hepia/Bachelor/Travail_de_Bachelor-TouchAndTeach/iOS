import UIKit
import SlideMenuControllerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /**
     Create the slide menu and all view controller contained
     */
    private func createMenu() {
        //Creation of the menu and the main view controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        SlideMenuOptions.opacityViewBackgroundColor = UIColor.clearColor()
        SlideMenuOptions.contentViewScale = 1
        
        SlideMenuOptions.simultaneousGestureRecognizers = false
        SlideMenuOptions.leftViewWidth += 20
        
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("MainController") as! MainController
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenuController") as! LeftMenuController
        let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenuController") as! RightMenuController
        
        leftViewController.mainController = mainViewController
        
        //Create and show the Slide Menu
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        UINavigationBar.appearance().tintColor = Colors.primaryColor
        
        let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    /**
     Load a document from his URL
     
     - parameter url: URL of the document
     */
    func loadDocument(fromURL url: NSURL) {
        do {
            try Global_iOS.sharedInstance.documentController.currentDocument = Document(fromFile: url)
        } catch {}
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //If the communication center is o
        if Global_iOS.sharedInstance.commCenter == nil {
            Global_iOS.sharedInstance.commCenter = CommCenter()
        }
        
        //Create the slide menu
        self.createMenu()
        
        if let url = launchOptions?[UIApplicationLaunchOptionsURLKey] as? NSURL {
            self.loadDocument(fromURL: url)
        }
        
        return true
    }
    
    func application(application: UIApplication, handleOpenURL url: NSURL) -> Bool {
        self.loadDocument(fromURL: url)
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //If the communication center is open, disconnect from the wall
        if let commCenter = Global_iOS.sharedInstance.commCenter {
            commCenter.disconnect()
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}