import Foundation
import UIKit

import SCLAlertView

/**
 The LeftMenuController class is the view controller of left menu.
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class LeftMenuController: MenuController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var mainController: MainController?
    
    var sectionsTitles: [String] = []
    var menuElements: [[MenuElement]] = [[]]
    
    var allScreen: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Define titles and content of the menu
        sectionsTitles = [NSLocalizedString("MENU_TITLE_MODES", comment: "MENU_TITLE_MODES"), NSLocalizedString("MENU_TITLE_DOCUMENTS", comment: "MENU_TITLE_DOCUMENTS"), NSLocalizedString("MENU_TITLE_SETTINGS", comment: "MENU_TITLE_SETTINGS")]
        menuElements = [[MenuElement(image: UIImage(named: "iconSlide")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_NORMAL_MODE", comment: "MENU_ITEM_NORMAL_MODE"), action: self.normalMode),
                        MenuElement(image: UIImage(named: "iconAvailableDraws")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_AVAILABLE_DRAWS", comment: "MENU_ITEM_AVAILABLE_DRAWS"), action: self.availableDraws),
                        MenuElement(image: UIImage(named: "iconSendSlide")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_SEND_SLIDE", comment: "MENU_ITEM_SEND_SLIDE"), action: self.sendSlide),
                        MenuElement(image: UIImage(named: "iconSendPhoto")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_SEND_PHOTO", comment: "MENU_ITEM_SEND_PHOTO"), action: self.sendPhoto)]
                        ,
                        [MenuElement(image: UIImage(named: "iconDocument")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_DOC_NEW", comment: "MENU_ITEM_DOC_NEW"), action: self.newDocument),
                        MenuElement(image: UIImage(named: "iconName")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_DOC_NAME", comment: "MENU_ITEM_DOC_NAME"), action: self.changeCurrentDocumentName),
                        MenuElement(image: UIImage(named: "iconSave")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_DOC_SAVE", comment: "MENU_ITEM_DOC_SAVE"), action: self.saveCurrentDocument),
                        MenuElement(image: UIImage(named: "iconLoad")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_DOC_LOAD", comment: "MENU_ITEM_DOC_LOAD"), action: self.loadDocument)]
                        ,
                        [MenuElement(image: UIImage(named: "iconSettings")!.image(withColor: Colors.menuSecondaryColor), title: NSLocalizedString("MENU_ITEM_SETTINGS", comment: "MENU_ITEM_SETTINGS"), action: self.openSettings)]]
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.allScreen = view.superview!.superview!
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sectionsTitles.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuElements[section].count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionsTitles[section]
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as! MenuCell
        
        cell.prepareCell(withMenuElement: self.menuElements[indexPath.section][indexPath.row])

        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.cellForRowAtIndexPath(indexPath)?.backgroundColor = Colors.menuTableViewSecondaryColor
        
        //Execute the action linked to the menu item
        self.menuElements[indexPath.section][indexPath.row].executeAction()
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.cellForRowAtIndexPath(indexPath)?.backgroundColor = UIColor.clearColor()
    }
    
    /**
     Go to the main controller
     */
    func normalMode() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if self.mainController == nil {
            self.mainController = (storyboard.instantiateViewControllerWithIdentifier("MainController") as! MainController)
        }
        
        self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: self.mainController!), close: true)
    }
    
    /**
     Open settings view controller
     */
    func openSettings() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let settingsController = storyboard.instantiateViewControllerWithIdentifier("SettingsController") as! SettingsController
        self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: settingsController), close: true)
    }
    
    /**
     Open the available draws view for select which draw to import
     */
    func availableDraws() {
        dispatch_async(dispatch_get_main_queue(), {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let availableDrawsController = storyboard.instantiateViewControllerWithIdentifier("AvailableDrawsController") as! AvailableDrawsController
            availableDrawsController.modalPresentationStyle = .FormSheet
            
            availableDrawsController.onSelectionAction = self.availableDrawsClosed
            
            self.presentViewController(availableDrawsController, animated: true, completion: nil)
        })
    }
    
    /**
     Called when the available draw view is closed
     
     - parameter selectedDrawName: Possible name of the draw which the user want to import
     */
    func availableDrawsClosed(withSelectedDrawName selectedDrawName: String?) {
        //Test if the user want to import a draw
        if let selectedDrawName = selectedDrawName {
            self.allScreen.makeToastActivity(.Center)
            NSTimer.scheduledTimerWithTimeInterval(5.0, target: self.allScreen, selector: #selector(allScreen.hideToastActivity), userInfo: nil, repeats: false)
            
            //Load the draw from the server
            Global_iOS.sharedInstance.commCenter?.askDraw(withName: selectedDrawName, withCompletionHandler: self.drawReceived)
        } else {
            //Display no connection error
            SCLAlertView().showCustom(NSLocalizedString("AVDRAWS_NO_CONNECTION_TITLE", comment: "AVDRAWS_NO_CONNECTION_TITLE"), subTitle: NSLocalizedString("AVDRAWS_NO_CONNECTION_MSG", comment: "AVDRAWS_NO_CONNECTION_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconConnectionState_Disconnected")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("OK", comment: "OK"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
        }
    }
    
    /**
     Called when a draw is received by the application
     
     - parameter drawJsonString: The Json received from the wall
     */
    func drawReceived(withJsonString drawJsonString: String) {
        dispatch_async(dispatch_get_main_queue(), {
            //Deserialize the json and create an array of draw object
            do {
                let jsonObject = try NSJSONSerialization.JSONObjectWithData(drawJsonString.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions.MutableContainers)
                
                Global_iOS.sharedInstance.documentController.currentDocument.createSlide(fromJson: jsonObject, addItToSlidesList: true, setItLikeCurrentSlide: true)
            } catch {
                self.view.makeToast("")
            }
            
            self.allScreen.hideToastActivity()
        })
    }
    
    /**
     Send the current slide
     */
    func sendSlide() {
        Global_iOS.sharedInstance.commCenter?.send(oneSlide: Global_iOS.sharedInstance.documentController.currentDocument.currentSlide)
        
        self.slideMenuController()?.closeLeft()
    }
    
    /**
     Open the photo controller for send a picture
     */
    func sendPhoto() {
        dispatch_async(dispatch_get_main_queue(), {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let photoController = storyboard.instantiateViewControllerWithIdentifier("PhotoController") as! PhotoController
            photoController.modalPresentationStyle = .FormSheet
            
            self.presentViewController(photoController, animated: true, completion: nil)
        })
    }
    
    /**
     Create a new document
     */
    func newDocument() {
        let alert = SCLAlertView()
        alert.addButton(NSLocalizedString("YES", comment: "YES")) {
            Global_iOS.sharedInstance.documentController.currentDocument = Document()
        }
        alert.showCustom(NSLocalizedString("ATTENTION", comment: "ATTENTION"), subTitle: NSLocalizedString("DOCUMENT_NEW_MSG", comment: "DOCUMENT_NEW_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconDocument")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
    }
    
    /**
     Ask the user to change the document name
     */
    func changeCurrentDocumentName() {
        Global_iOS.sharedInstance.documentController.currentDocument.rename()
    }
    
    /**
     Ask the user to save the document by first ask to validate the name
     */
    func saveCurrentDocument() {
        DocumentsManager.sharedInstance.askSave(document: Global_iOS.sharedInstance.documentController.currentDocument, onView: self.view)
    }
    
    /**
     Show the Document List Controller for select a document to show
     */
    func loadDocument() {
        dispatch_async(dispatch_get_main_queue(), {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let documentsLoaderController = storyboard.instantiateViewControllerWithIdentifier("DocumentsLoaderController") as! DocumentsLoaderController
            documentsLoaderController.modalPresentationStyle = .FormSheet
            
            documentsLoaderController.onDocumentLoadedAction = self.newDocumentLoaded
            
            self.presentViewController(documentsLoaderController, animated: true, completion: nil)
        })
    }
    
    /**
     Function called when the document have been loaded
     */
    func newDocumentLoaded() {
        self.view.makeToast(NSLocalizedString("DOCUMENT_LOADED", comment: "DOCUMENT_LOADED"))
    }
}