import Foundation

/**
 Description
 ---
 Convert (or Serialize) an object to a JSON String or Dictionary.
 
 Usage
 ---
 Use 'Serialize.toJSON' on instances of classes that:
 0. Inherit from NSObject
 0. Implement 'Serializable' protocol
 0. Implement the property 'jsonProperties' and return an array of strings with names of all the properties to be serialized
 
 Changements from original code
 ---
 * Convert to Swift 2.2 (Original is in Swift 1.x)
 * Convert function names into Swift 3 API Design Guidelines
 * Modification of "if ... is Class" test of type by "if let ... as? Class" right Swift method
 * Bug fix (in function toDictionnary the test of type Serializable convert the value in Json String but have to convert in dictionnary)
 * Optimisations
 
 - note: This is the protocol to implement for being serializable
 - authors: Michaël Minelli, Ahmad AlNaimi
 - seealso: [Original code](https://gist.github.com/anaimi/ad336b44d718430195f8)
 - version: 2.0.0
 */
@objc protocol Serializable {
    var jsonProperties:Array<String> { get }
    func jsonValue(forKey key: String!) -> AnyObject?
}