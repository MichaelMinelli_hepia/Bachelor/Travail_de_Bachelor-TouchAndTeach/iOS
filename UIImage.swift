import Foundation
import UIKit

// Extension of the UIImage class from UIKit
extension UIImage {
    /**
     Get a rotated copy of the current UIImage based of his orientation informations
    */
    var rotatedCopy: UIImage {
        if (imageOrientation == UIImageOrientation.Up) {
            return self
        }
        
        UIGraphicsBeginImageContext(self.size)
        
        self.drawInRect(CGRect(origin: CGPoint.zero, size: self.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy
    }
    
    /**
     Recreate the current image but with a new color
     
     - parameter tintColor: The new color of the image
     
     - returns: Image with new color
     */
    func image(withColor tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()! as CGContextRef
        CGContextTranslateCTM(context, 0, self.size.height)
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        CGContextClipToMask(context, rect, self.CGImage)
        tintColor.setFill()
        CGContextFillRect(context, rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    /**
     Create a new image with the wanted size
     
     - parameter newSize: The new size
     
     - returns: The resized image
     */
    func image(scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        self.drawInRect(CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}