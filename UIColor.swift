import Foundation
import UIKit

// Extension of the UIColor class from UIKit
extension UIColor {
    /**
     Transform the current color into an Int value
     
     - note: The 32 bytes are composed like that : Alpha - Red - Green - Blue
    */
    var argb: UInt64? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = UInt64(fRed * 255.0)
            let iGreen = UInt64(fGreen * 255.0)
            let iBlue = UInt64(fBlue * 255.0)
            let iAlpha = UInt64(fAlpha * 255.0)
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
            
            return rgb
        } else {
            // Could not extract RGBA components
            return nil
        }
    }
}