import Foundation

import Foundation

/**
 The Stack struct is an implementation of a Stack based on a simple table
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
struct Stack<Element> {
    private var items = [Element]()
    
    /**
     Add an element on the top of the stack
     
     - parameter item: Element to add on the top
     */
    mutating func push(item: Element) {
        self.items.append(item)
    }
    
    /**
     Remove the top element of the stack
     
     - returns: The element removed
     */
    mutating func pop() -> Element? {
        return self.items.count > 0 ? self.items.removeLast() : nil
    }
    
    /**
     Clear the stack
     */
    mutating func removeAllObjects() {
        self.items.removeAll()
    }
}