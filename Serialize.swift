import Foundation
import UIKit

/**
 Description
 ---
 Convert (or Serialize) an object to a JSON String or Dictionary.
 
 Usage
 ---
 Use 'Serialize.toJSON' on instances of classes that:
    0. Inherit from NSObject
    0. Implement 'Serializable' protocol
    0. Implement the property 'jsonProperties' and return an array of strings with names of all the properties to be serialized
 
 Changements from original code
 ---
 * Convert to Swift 2.2 (Original is in Swift 1.x)
 * Convert function names into Swift 3 API Design Guidelines
 * Add Array converter
 * Modification of "if ... is Class" test of type by "if let ... as? Class" right Swift method
 * Bug fix (in function toDictionnary the test of type Serializable convert the value in Json String but have to convert in dictionnary)
 * Optimisations
 
 - note: This struct contains only static property
 - authors: Michaël Minelli, Ahmad AlNaimi
 - seealso: [Original code](https://gist.github.com/anaimi/ad336b44d718430195f8)
 - version: 2.0.0
 */
struct Serialize {
    /**
     Transform a Serializable Object into Dictionnary
     
     - parameter obj: Object to transform
     
     - returns: Dictionnary generated
     */
    static func toDictionary(fromSerializable obj:Serializable) -> NSDictionary {
        var dict = Dictionary<String, AnyObject>()
        
        //For all property specified in the object list
        for prop in obj.jsonProperties {
            //Get the value
            let val:AnyObject? = obj.jsonValue(forKey: prop)
            
            //Convert and add the value to the dictionnary
            if let val = val {
                if let val = val as? Serializable
                {
                    dict[prop] = self.toDictionary(fromSerializable: val)
                }
                else if let val = val as? Array<Serializable>
                {
                    var arr = Array<NSDictionary>()
                    
                    for item in val {
                        arr.append(self.toDictionary(fromSerializable: item))
                    }
                    
                    dict[prop] = arr
                } else {
                    dict[prop] = val
                }
            } else {
                dict[prop] = nil
            }
        }
        
        return dict
    }
    
    /**
     Transform a Serializable Object into a JSON String
     
     - parameter obj: Object to transform
     
     - returns: JSON formatted String
     */
    static func toJSON(fromSerializable obj:Serializable) -> String {
        //Get a dictionnary from the object
        let dict = self.toDictionary(fromSerializable: obj)
        
        do {
            //Transform the dictionnary into a series of byte (JSON formated String in UTF-8)
            let data = try NSJSONSerialization.dataWithJSONObject(dict, options:NSJSONWritingOptions(rawValue: 0))
            
            //Decode the series of bytes
            return NSString(data: data, encoding: NSUTF8StringEncoding)! as String
        } catch { }
        
        return "{}"
    }
    
    /**
     Transform a array of Serializable Object into a JSON String
     
     - parameter arr: Object to transform
     
     - returns: JSON formatted String
     */
    static func toJSON(fromArray arr: Array<Serializable>) -> String {
        var exportable = [NSDictionary]()
        
        for element in arr {
            exportable.append(self.toDictionary(fromSerializable: element))
        }
        
        do {
            //Transform the array into a series of byte (JSON formated String in UTF-8)
            let data = try NSJSONSerialization.dataWithJSONObject(exportable, options:NSJSONWritingOptions(rawValue: 0))
            
            //Decode the series of bytes
            return NSString(data: data, encoding: NSUTF8StringEncoding)! as String
        } catch { }
        
        return "{}"
    }
}